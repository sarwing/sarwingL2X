from __future__ import print_function

import logging
import os
import traceback
from typing import Optional, List

from sarwingL2X.constants import *

import time
from importlib import import_module

from future import standard_library

standard_library.install_aliases()
import sys

sys.path.append("./L2C")
sys.path.append("./L2M")

from sarwingL2X import swL2XReport
from subprocess import call
import csv
import threading
import yaml
from sarwingL2X.plugins import PluginArgs

import os

qsubarray = os.getenv("QSUBARRAY", "")


def concat_ok(reportPath):
    # Get concat with OK status. Put them in a big string suitable for piping
    concatOkNc = []
    concatOkComment = []
    with open(os.path.join(reportPath, CONCAT_CSV)) as concatReport:
        reportDict = csv.DictReader(concatReport)
        for row in reportDict:
            concatDir = os.path.dirname(row["L2C file"])
            if not checkStatus(os.path.join(concatDir, CONCAT_STATUS)):
                # status OK : need can be post processed
                concatOkNc.append(row["L2C file"])
                concatOkComment.append(row["Comments"])
    return concatOkNc, concatOkComment


def l2m_ok(report_path):
    with open(os.path.join(report_path, L2M_OK_TXT)) as l2m_report:
        l2m_lines = l2m_report.readlines()
    return l2m_lines


def l2m_res(report_path):
    with open(os.path.join(report_path, POST_PROC_FILE)) as post_proc_file:
        post_proc_lines = post_proc_file.readlines()

    for l in post_proc_lines:
        if l.startswith("L2M"):
            res_str = l.replace("L2M ", "")
            res = res_str.split(" ")
            return res
    return None


def checkStatus(statusPath, status_ok=["0"]):
    # warning: return *False* if status is ok
    if os.path.isfile(statusPath):
        try:
            logging.debug("Trying to open status file %s ..." % statusPath)
            f = open(statusPath, "r")
        except IOError:
            logging.debug("Error opening status file. Generation will be forced.")
            return True
        else:
            with f:
                status = f.readline().strip()
                if status in status_ok:
                    logging.debug("Status OK")
                    return False
                else:
                    logging.debug("Status is %s, generation will be forced" % status)
                    return True
    else:
        logging.debug("No status file, generation will be forced.")
        return True


def checkVersion(versionPath, currVersion):
    if os.path.isfile(versionPath):
        try:
            logging.debug("Trying to open version file %s ..." % versionPath)
            f = open(versionPath, "r")
        except IOError:
            logging.debug("Error opening version file. Generation will be forced.")
            return True
        else:
            with f:
                ver = f.readline().strip()
                if ver == currVersion:
                    logging.debug(
                        "Version file open, versions are the same : %s. Generation will not be forced"
                        % ver
                    )
                    return False
                else:
                    logging.debug(
                        "Version file open, versions are different, previous : %s, newest : %s. Generation will be forced."
                        % (ver.strip(), currVersion.strip())
                    )
                    return True
    else:
        logging.debug("No version file, generation will be forced.")
        return True


stop_thread = False

# Initialize threading events
update_event = threading.Event()
update_requested = threading.Event()


def l2xProcessor(
    config_file: str,
    outDir: str = ".",
    force: bool = False,
    reportPath: str = ".",
    debug: bool = False,
    plugins_to_exec: Optional[List[str]] = None,
) -> None:
    """
    Process data using plugins for L2X processing.

    Parameters
    ==========
        outDir (str, optional): Output directory. Defaults to ".".
        force (bool, optional): Whether to force processing. Defaults to False.
        report_path (str, optional): Report path. Defaults to ".".
        debug (bool, optional): Whether to enable debug mode. Defaults to False.

    Returns
    =======
        None
    """
    global stop_thread

    logging.info(f"Config file: {config_file}")
    with open(config_file, "r") as stream:
        try:
            config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
            sys.exit(1)

    plugins = []
    if "plugins" in config:
        for p in config["plugins"]:
            if isinstance(p, str):
                plugins.append(p)
            elif isinstance(p, dict):
                plugins.append(next(iter(p)))
    logging.info(f"L2X PLUGINS: {plugins}")

    pluginsForce = []
    if "force_plugin" in config:
        for p in config["plugins"]:
            if isinstance(p, str):
                pluginsForce.append(p)

    # TODO manage when no resolution is given with L2M
    standalone_plugins = []
    with open(os.path.join(reportPath, POST_PROC_FILE), "w") as postProcFile:
        if plugins:
            for plugin in config["plugins"]:
                if isinstance(plugin, str):
                    # Write the plugin name directly if it's a string
                    to_w = plugin
                elif isinstance(plugin, dict):
                    # If the plugin is a dictionary, we need to handle it differently
                    plugin_name = next(iter(plugin))

                    # standalone plugins are written to another file
                    if plugin[plugin_name].get("standalone", False):
                        standalone_plugins.append(
                            plugin[plugin_name].get("id", plugin_name)
                        )
                        continue

                    if plugin_name == "L2M":
                        l2m_res = plugin["L2M"].get("resolutions")
                        if l2m_res:
                            # Join the resolutions into a string if they exist
                            to_w = "L2M " + " ".join(map(str, l2m_res))
                        else:
                            msg = "No resolutions found in config for L2M plugin. Exiting."
                            logging.error(msg)
                            raise KeyError(msg)
                    elif "id" in plugin[plugin_name]:
                        to_w = plugin[plugin_name].get("id")
                    else:
                        # If it's another plugin dictionary, write its name
                        to_w = plugin_name
                else:
                    msg = "Unexpected plugin format. Exiting."
                    logging.error(msg)
                    raise ValueError(msg)

                postProcFile.write(to_w + "\n")

    # Write standalone plugins into another file
    with open(
        os.path.join(reportPath, POST_PROC_STANDALONE_FILE), "w"
    ) as postProcFile_std:
        for std_p in standalone_plugins:
            postProcFile_std.write(std_p + "\n")

    dbg = ""
    if debug:
        dbg = "--debug"
    frc = ""
    if force:
        frc = "-f"

    def backgrd_report():
        global stop_thread
        while not stop_thread:
            # Perform report update
            swL2XReport.buildReport(reportPath, config_file)
            # Signal that the report has been updated
            update_event.set()
            # Wait for either an update request or timeout
            sleep_time = config["update_report_sleep_time_seconds"]
            update_requested.wait(timeout=sleep_time)
            # Clear the update_requested event if it's set
            update_requested.clear()

    t = threading.Thread(target=backgrd_report)

    post_ssh_cmd = config.get("post_ssh_cmd", False)
    if post_ssh_cmd:
        post_ssh_cmd = post_ssh_cmd + " && "
    else:
        post_ssh_cmd = ""

    if False and not config["skip_l2c"]:
        commit = reportPath.rstrip("/").split("/")[config["commit_index"]]
        l2c_env = os.path.join(config["conda_envs_dir"], commit, "l2x")

        if config["use_qsubarray_default"]:
            insert_qsub = f'export QSUBARRAY=\\"{qsubarray}\\" &&'
        else:
            insert_qsub = ""

        if config["ssh_host_default"]:
            condash_location = config["condash_source_location_remote"]
            ssh_host = config["ssh_host_default"]
            ssh_cmd = f"rsh {ssh_host} -o SendEnv=QSUBARRAY \"bash -c '"
            ssh_cmd_end = "'\""
        else:
            condash_location = config["condash_source_location"]
            ssh_cmd = ""
            ssh_cmd_end = ""

        cmd_str = (
            f"{ssh_cmd} {post_ssh_cmd} source {condash_location} "
            f"&& conda activate {l2c_env}/ && {insert_qsub} "
            f"swConcatListing.py -d %s --reportPath %s %s %s {ssh_cmd_end}"
            % (outDir, reportPath, dbg, frc)
        )
        logging.info(f"Concatenation command: {cmd_str}")
        # Concatenation process
        call(cmd_str, shell=True, executable="/bin/bash")

    if reportPath:
        # L2C and L2M is handled separately, start report process directly.
        t.start()
        standalone_plugin_dir = os.path.join(reportPath, STANDALONE_PLUGINS_SUBDIR)
        os.makedirs(standalone_plugin_dir, exist_ok=True)
        listing_name_subpath = config["listing_name_file_relative_to_report"]
        commit = reportPath.rstrip("/").split("/")[config["commit_index"]]
        config_name = reportPath.rstrip("/").split("/")[config["config_index"]]
        listing_name = reportPath.split("/")[config["listing_name_index"]]

        # Run plugins
        for p in config["plugins"]:
            # Check if the plugin is a string or dictionary
            if isinstance(p, str):
                p_name = p
                p_config = {}  # Default to an empty config if it's just a name
            elif isinstance(p, dict):
                # If it's a dictionary, get the name and the config
                p_name, p_config = list(p.items())[0]
            else:
                # Log some error if the format is unexpected
                logging.error("Plugin configuration format is not recognized.")
                continue

            if p_name == "L2M":
                # L2M is now handled separately
                continue

            # Dynamically import the module
            module = import_module(f"sarwingL2X.plugins.{p_name}")

            conda_env_path = "default"
            if "conda_env_name" in p_config:
                # The conda_envs_dir and commit should be defined earlier in your script
                conda_env_path = os.path.join(
                    config["conda_envs_dir"], commit, p_config["conda_env_name"]
                )

            if not plugins_to_exec or (plugins_to_exec and p_name in plugins_to_exec):
                # Check if plugin requires report update before execution
                wait_for_report = p_config.get('wait_for_report_update', False)
                if wait_for_report:
                    logging.info(f"Waiting for report update before executing {p_name}")
                    # Request immediate report update
                    update_requested.set()
                    # Wait until the report thread signals completion
                    update_event.wait()
                    update_event.clear()

                # Get the function from the module
                plugin_function = getattr(module, p_name)
                p_args = PluginArgs(
                    plugin=p_name,
                    out_dir=outDir,
                    force=force or p_name in pluginsForce,
                    force_str=frc,
                    report_path=reportPath,
                    listing_name=listing_name,
                    config=config,
                    commit=commit,
                    config_name=config_name,
                    plugin_config=p_config,
                    standalone_plugin_dir=standalone_plugin_dir,
                    debug=dbg,
                    custom_conda_env=str(conda_env_path),
                    use_qsubarray_default=config["use_qsubarray_default"],
                    ssh_host_default=config["ssh_host_default"],
                    post_ssh_cmd=post_ssh_cmd,
                )

                try:
                    # Execute the function
                    plugin_function(p_args)
                except BaseException as e:
                    logging.error(f"Exception while executing {p_name}")
                    logging.error(traceback.format_exc())

            # The update report thread is started only after L2M, because it doesn't work before L2M has been generated.
            # This is legacy when L2M was generated through L2X.
            # if p_name == "L2M":
            #    t.start()

    # Stopping report thread
    stop_thread = True
    # Ensure any waiting is interrupted
    update_requested.set()
    t.join()

    logging.info("Building HTML L2X report...")
    swL2XReport.buildReport(reportPath, config_file)
    logging.info("Report generated. L2X process is over.")
