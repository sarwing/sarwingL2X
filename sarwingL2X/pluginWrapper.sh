#!/bin/bash

echo "$(date '+%Y-%m-%d %H:%M:%S') - Executing: $0 $@"

[ -z "$verbose_level" ] && verbose_level=1

function verbose {
    #set -x
    local msg="$1"
    local level="${2:-1}"
    verbose_level=${verbose_level:-1}
    (( $level <= $verbose_level )) && echo -e "$msg" >&2
    #set +x
}

# Call like this :
# pluginWrapper.sh "command_plugin_exec" "plugin_name" "path_in" "path_out" "identifier" "arg1_to_plugin" "arg2_to_plugin" ...

pluginPath="$1"; shift
pluginName="$1"; shift
pathIn="$1"; shift
pathOut="$1"; shift # PathOut is relative to pathin
identifier="$1"; shift # Identifier is a unique value used to name log, status and version file.

# If pathIn is an existing file, pathOut is computed being relative to pathIn dirname
if [ -f "$pathIn" ]; then
  pathOut="$(dirname "$pathIn")/$pathOut"
else # Else, pathIn is a directory, so we don't have to use dirname (and pathOut is still relative to pathIn)
  pathOut="$pathIn/$pathOut"
fi

## Check if custom conda env has to be used
#if [[ "$custom_conda_env" != "default" ]]; then
#  if [ -d "$custom_conda_env" ] && [ -f "$custom_conda_env/bin/activate" ]; then
#    verbose "Activating $custom_conda_env"
#    source activate "$custom_conda_env"
#  else
#    verbose "Conda environment does not exist at $custom_conda_env"
#    echo 70 > "$pathOut/$identifier.status"
#    exit 70
#  fi
#fi

#all_args="$@" # Collect remaining arguments

# For additionnal args
#cmdArgs=${@:4}

mkdir -p "$pathOut"

# empty status is invalid
[ ! -f "$pathOut/$identifier.status" ] && echo 1 >  "$pathOut/$identifier.status"
[ -f "$pathOut/$identifier.status" ] && [ ! -s "$pathOut/$identifier.status" ] && echo 1 >  "$pathOut/$identifier.status"

if [ ! -f "$pathOut/$identifier.status" ] && [ -f "$pathOut/$identifier.log" ] && (( ($(date +%s) - $(date -r "$pathOut/$identifier.log" +%s))/3600 > 10 )); then
    # old job that never returned. assume bad status
    echo 240 > "$pathOut/$identifier.status"
fi

verbose "version file : $pathOut/$identifier.version"
if [ -f "$pathOut/$identifier.version" ] ;then
    #set -x
    # does the current version is newer ? ( plugin )
    cur_ver=$(cat "$pathOut/$identifier.version")
    [ -z "$cur_ver" ] && verbose "warning: empty ver in $pathOut/$identifier.version"

    new_ver=$($pluginPath --version)

    if [ -z "$cur_ver" ] || [[ "$cur_ver" < "$new_ver" ]] ; then
        # new version available
        verbose "new version $new_ver available"
        echo 220 > "$pathOut/$identifier.status"
    fi
else
  echo 230 > "$pathOut/$identifier.status"
fi

status=$(cat "$pathOut/$identifier.status")
verbose "Previous status: $status"

# Status not OK, running plugin
#if [[ "$status" -ne  0 ]] ; then
# Here we try to catch "real" failures.
if [[ "$status" -eq 1 || "$status" -eq 2 || ("$status" -ge 126 && "$status" -le 139) || "$status" -eq 130 || "$status" -eq 220 || "$status" -eq 230 || "$status" -eq 240 ]] ; then
  verbose "Executing $pluginName. Previous status was $status"
  echo "Command: $pluginPath $@"
  # Executing plugin and redirecting error and stdout to log file
  $pluginPath "$@" > "$pathOut/$identifier.log" 2>&1
  # $pluginPath $pathIn $pathOut > "$pathOut/$pluginName.log" 2>&1
  status=$?
  # Writing status to file
  echo $status > "$pathOut/$identifier.status"

  # Writing plugin version to file
  $pluginPath --version > "$pathOut/$identifier.version"

  cat "$pathOut/$identifier.log"
fi

exit $status

