from . import PluginArgs
from ..swL2XProcessor import concat_ok
import os
from subprocess import Popen, PIPE
import logging

logger = logging.getLogger(os.path.basename(__file__))
qsubarray=os.getenv("QSUBARRAY", "")


def nclight_deprecated(args: PluginArgs):
    print("Found plugin %s" % args.plugin)
    pluginBasePath = "/home1/datahome/oarcher/sarwing_cluster/post_processing/"
    pluginPath = os.path.join(pluginBasePath, "nclight")

    concatOkNc, concatOkComment = concat_ok(args.report_path)

    concatLines = '\n'.join([os.path.dirname(ncPath) for ncPath in concatOkNc])

    # pluginWrapper.sh "command_plugin_exec" "plugin_name" "path_in" "path_out" "conda_env_path"
    # "PATH_IN" "PATH_OUT" ...
    cmdStr = (f"{qsubarray} xargs -l1 -r -I PATH pluginWrapper.sh {pluginPath} {args.plugin} "
              f"PATH post_processing/{args.plugin} default "
              f"PATH PATH/post_processing/{args.plugin}")
    logger.info("Starting %s plugin execution..." % args.plugin)
    p = Popen(cmdStr, stdin=PIPE, shell=True)
    p.communicate(input=concatLines.encode("utf-8"))
    logger.info("Finished %s execution." % args.plugin)
