import logging

logger = logging.getLogger(__name__)

import time
from . import PluginArgs
import os
import glob
import subprocess
import psycopg2
from psycopg2 import sql
import zlib
import threading  # New import for handling threads
import traceback


def get_foreign_key_constraints(cur, schema, table):
    """
    Retrieve foreign key constraints for a given table in a specified schema.
    Parameters:
    cur (cursor): The database cursor to execute the SQL command.
    schema (str): The name of the schema containing the table.
    table (str): The name of the table for which to retrieve foreign key constraints.
    Returns:
    list of tuples: A list of tuples where each tuple contains the following information about a foreign key constraint:
        - constraint_name (str): The name of the foreign key constraint.
        - column_names (list of str): The list of column names in the table that are part of the foreign key.
        - foreign_table_schema (str): The schema name of the foreign table.
        - foreign_table_name (str): The name of the foreign table.
        - foreign_column_names (list of str): The list of column names in the foreign table that are referenced by the foreign key.
        - update_rule (str): The update rule for the foreign key (CASCADE, SET NULL, SET DEFAULT, RESTRICT, NO ACTION).
        - delete_rule (str): The delete rule for the foreign key (CASCADE, SET NULL, SET DEFAULT, RESTRICT, NO ACTION).
    """

    sql_cmd = """
    SELECT
        con.conname AS constraint_name,
        array_agg(att2.attname ORDER BY k.n) AS column_names,
        nsf.nspname AS foreign_table_schema,
        confrel.relname AS foreign_table_name,
        array_agg(att.attname ORDER BY k.n) AS foreign_column_names,
        CASE con.confupdtype
            WHEN 'c' THEN 'CASCADE'
            WHEN 'n' THEN 'SET NULL'
            WHEN 'd' THEN 'SET DEFAULT'
            WHEN 'r' THEN 'RESTRICT'
            WHEN 'a' THEN 'NO ACTION'
        END AS update_rule,
        CASE con.confdeltype
            WHEN 'c' THEN 'CASCADE'
            WHEN 'n' THEN 'SET NULL'
            WHEN 'd' THEN 'SET DEFAULT'
            WHEN 'r' THEN 'RESTRICT'
            WHEN 'a' THEN 'NO ACTION'
        END AS delete_rule
    FROM
        pg_constraint con
        JOIN pg_class rel ON rel.oid = con.conrelid
        JOIN pg_namespace ns ON ns.oid = rel.relnamespace
        JOIN pg_class confrel ON confrel.oid = con.confrelid
        JOIN pg_namespace nsf ON nsf.oid = confrel.relnamespace
        CROSS JOIN LATERAL generate_subscripts(con.conkey, 1) AS k(n)
        JOIN pg_attribute att ON att.attrelid = confrel.oid AND att.attnum = con.confkey[k.n]
        JOIN pg_attribute att2 ON att2.attrelid = rel.oid AND att2.attnum = con.conkey[k.n]
    WHERE
        con.contype = 'f'
        AND ns.nspname = %s
        AND rel.relname = %s
    GROUP BY
        con.conname, nsf.nspname, confrel.relname, con.confupdtype, con.confdeltype
    ORDER BY
        con.conname;
    """
    cur.execute(sql_cmd, (schema, table))
    return cur.fetchall()


def recreate_foreign_keys(
    cur, schema_hash_hex, table, constraints, tables_in_new_schema
):
    """
    Recreate foreign key constraints in the new schema.

    Parameters:
        cur (cursor): The database cursor to execute SQL commands.
        schema_hash_hex (str): The name of the new schema.
        table (str): The name of the table to add foreign keys to.
        constraints (list of tuples): The foreign key constraints to recreate.
        tables_in_new_schema (list of str): List of tables that are in the new schema.

    Returns:
        None
    """
    for constraint in constraints:
        constraint_name = constraint[0]
        column_names = constraint[1]  # list of local column names
        foreign_table_schema = constraint[2]
        foreign_table_name = constraint[3]
        foreign_column_names = constraint[4]  # list of foreign column names
        update_rule = constraint[5]
        delete_rule = constraint[6]

        # Determine the schema of the referenced table
        if foreign_table_name in tables_in_new_schema:
            # The referenced table is in the new schema
            new_foreign_schema = schema_hash_hex
        else:
            # The referenced table remains in its original schema
            new_foreign_schema = foreign_table_schema

        # Convert column names to comma-separated lists
        local_columns = ", ".join([f'"{col}"' for col in column_names])
        foreign_columns = ", ".join([f'"{col}"' for col in foreign_column_names])

        # Construct the ALTER TABLE statement to add the foreign key
        sql_cmd = sql.SQL(
            """
            ALTER TABLE {}.{}
            ADD CONSTRAINT {} FOREIGN KEY ({})
            REFERENCES {}.{} ({})
            ON UPDATE {} ON DELETE {};
        """
        ).format(
            sql.Identifier(schema_hash_hex),
            sql.Identifier(table),
            sql.Identifier(constraint_name),
            sql.SQL(local_columns),
            sql.Identifier(new_foreign_schema),
            sql.Identifier(foreign_table_name),
            sql.SQL(foreign_columns),
            sql.SQL(update_rule),
            sql.SQL(delete_rule),
        )
        logger.debug(f"Adding foreign key constraint: {constraint_name}")
        logger.debug(sql_cmd)
        cur.execute(sql_cmd)


def copy_triggers(cur, schema_hash_hex, table):
    """
    Copy triggers from the public schema to the new schema for the specified table.

    Parameters:
        cur (cursor): The database cursor to execute SQL commands.
        schema_hash_hex (str): The name of the new schema.
        table (str): The name of the table whose triggers are to be copied.

    Returns:
        None
    """
    sql_cmd = sql.SQL(
        """
        SELECT 
            pg_get_triggerdef(t.oid) || ';' as trigger_creation
        FROM pg_trigger t
        JOIN pg_class c ON t.tgrelid = c.oid
        JOIN pg_namespace n ON c.relnamespace = n.oid
        WHERE n.nspname = 'public' 
        AND c.relname = %s 
        AND NOT t.tgisinternal
    """
    )
    cur.execute(sql_cmd, [table])
    triggers = cur.fetchall()

    for trigger in triggers:
        # Replace schema name in trigger definition
        trigger_def = trigger[0].replace(" ON public.", f" ON {schema_hash_hex}.")
        logger.debug(f"Creating trigger: {trigger_def}")
        cur.execute(sql.SQL(trigger_def))


def terminate_all_connections(cur, db_name):
    """
    Terminate all active connections to the specified database except the current one.

    Parameters:
        cur (cursor): The database cursor to execute SQL commands.
        db_name (str): The name of the database.

    Raises:
        psycopg2.Error: If any database operation fails.
    """
    try:
        logger.info("Terminating all active connections to the database.")

        # Terminate all connections except the current one
        terminate_query = sql.SQL(
            """
            SELECT pg_terminate_backend(pid)
            FROM pg_stat_activity
            WHERE datname = %s
              AND pid <> pg_backend_pid();
        """
        )
        cur.execute(terminate_query, (db_name,))
        terminated = cur.fetchall()
        logger.info(f"Terminated connections: {terminated}")

    except psycopg2.Error as e:
        logger.error(f"Failed to terminate connections: {e}")
        raise


def drop_schema_with_timeout(cur, schema, timeout, db_name):
    """
    Attempt to drop a schema with a timeout. If the operation takes longer than
    the specified timeout, cancel the operation, terminate all database connections,
    and retry dropping the schema once more.

    Parameters:
        cur (cursor): The database cursor to execute SQL commands.
        schema (str): The name of the schema to drop.
        timeout (int): The maximum time (in seconds) to wait for the DROP SCHEMA command.
        db_name (str): The name of the database.

    Raises:
        TimeoutError: If the DROP SCHEMA command exceeds the specified timeout after retrying.
        psycopg2.Error: If any other database error occurs.
    """

    def attempt_drop():
        try:
            cur.execute(
                sql.SQL("DROP SCHEMA IF EXISTS {} CASCADE").format(
                    sql.Identifier(schema)
                )
            )
            return True
        except Exception as e:
            logger.error(f"Error dropping schema '{schema}': {e}")
            return False

    # First attempt to drop the schema
    event = threading.Event()
    drop_success = False

    def execute_drop_first_attempt():
        nonlocal drop_success
        drop_success = attempt_drop()
        event.set()

    drop_thread = threading.Thread(target=execute_drop_first_attempt)
    drop_thread.start()

    if not event.wait(timeout):
        logger.warning(
            f"First DROP SCHEMA '{schema}' attempt is taking longer than {timeout} seconds. Attempting to cancel..."
        )
        try:
            cur.connection.cancel()
            drop_thread.join()
            # Terminate all connections to prevent hanging
            terminate_all_connections(cur, db_name)
            logger.info("Terminating connections and preparing to retry DROP SCHEMA.")
        except Exception as cancel_e:
            logger.error(
                f"Failed to cancel DROP SCHEMA '{schema}' and terminate connections: {cancel_e}"
            )
            raise

        # Retry dropping the schema after terminating connections
        retry_event = threading.Event()
        drop_retry_success = False

        def execute_drop_retry():
            nonlocal drop_retry_success
            drop_retry_success = attempt_drop()
            retry_event.set()

        retry_drop_thread = threading.Thread(target=execute_drop_retry)
        retry_drop_thread.start()

        if not retry_event.wait(timeout):
            logger.error(
                f"Second DROP SCHEMA '{schema}' attempt exceeded timeout of {timeout} seconds."
            )
            retry_drop_thread.join()
            raise TimeoutError(
                f"DROP SCHEMA '{schema}' timed out after terminating connections."
            )

        if not drop_retry_success:
            logger.error(f"Second attempt to DROP SCHEMA '{schema}' failed.")
            raise TimeoutError(f"DROP SCHEMA '{schema}' failed after retrying.")

        logger.info(
            f"DROP SCHEMA '{schema}' succeeded on retry after terminating connections."
        )
    else:
        if not drop_success:
            logger.error(f"DROP SCHEMA '{schema}' failed on the first attempt.")
            raise psycopg2.Error(f"DROP SCHEMA '{schema}' failed.")
        logger.info(f"DROP SCHEMA '{schema}' succeeded on the first attempt.")


def cyclobs_db(args: PluginArgs):
    """
    Clone specific tables from the public schema to a new schema, including their constraints, triggers, and foreign keys.

    Parameters:
        args (PluginArgs): The arguments required for the plugin execution, including configuration and plugin details.

    Returns:
        None

    Raises:
        psycopg2.Error: If any database operation fails.
    """
    logger.info(f"Found plugin {args.plugin}")

    pg_admin_user = args.plugin_config["pg_admin_user"]
    pg_admin_password = args.plugin_config["pg_admin_password"]
    db_name = args.plugin_config["db_name"]
    db_owner = args.plugin_config["db_owner"]
    db_owner_password = args.plugin_config["db_owner_password"]
    db_host = args.plugin_config["db_host"]
    db_port = args.plugin_config["db_port"]
    drop_if_exists = args.plugin_config["drop_schema_if_exists"]
    create_if_not_exists = args.plugin_config["create_schema_if_not_exists"]
    add_update_args = args.plugin_config["cyclobs_db_update_args"]
    if "id" in args.plugin_config:
        p_id = args.plugin_config["id"]
    else:
        raise ValueError("Mandatory 'id' not found in the plugin configuration.")

    logger.info(f"Starting {p_id} plugin...")

    # Validate add_update_args
    valid_args = [
        "--no-sfmr-nesdis",
        "--no-sfmr-hrd",
        "--no-tcva",
        "--no-smos",
        "--no-smap",
        "--no-sar",
        "--no-coloc",
    ]

    if add_update_args:
        args_list = add_update_args.split()
        invalid_args = [arg for arg in args_list if arg not in valid_args]
        if invalid_args:
            raise ValueError(
                f"Invalid arguments found: {invalid_args}. Valid args are: {valid_args}"
            )

    commit = args.commit
    config_name = args.config_name
    condash_location = args.config["condash_source_location"]
    conda_env = args.custom_conda_env
    standalone_plugin_dir = args.standalone_plugin_dir
    proc_base_path = os.path.join(
        args.config["chain_topdir_data"], args.config["processing_subdir"]
    )
    condash_location = args.config["condash_source_location"]

    target_schema_name = f"{commit}_{config_name}_{args.listing_name}"
    schema_crc32_hash = zlib.crc32(target_schema_name.encode())
    schema_hash_hex = "cyclobs_" + format(schema_crc32_hash, "08x")
    database_url = (
        f"postgresql://{db_owner}:{db_owner_password}@{db_host}:{db_port}/{db_name}"
    )

    logger.info(
        f"Will create a schema with name {schema_hash_hex} for {commit}, {config_name}, {args.listing_name}"
    )

    # Establish a connection to the PostgreSQL server
    conn = psycopg2.connect(
        dbname=db_name,
        user=pg_admin_user,
        password=pg_admin_password,
        port=db_port,
        host=db_host,
    )
    # psycopg2.connect(dbname="postgres", user="postgres", password="sensation délitement forcené", port=5434, host="derive.ifremer.fr")
    logger.info("Connected to database.")

    conn.autocommit = True  # Needed to execute a CREATE DATABASE command
    status = False

    try:
        # Create a cursor to execute database commands
        cur = conn.cursor()

        if drop_if_exists:
            logger.info("Drop existing schema.")
            try:
                drop_schema_with_timeout(
                    cur,
                    schema_hash_hex,
                    timeout=30,  # Timeout set to 30 seconds
                    db_name=db_name,
                )
            except TimeoutError as te:
                logger.error(te)
                raise
            except psycopg2.Error as pe:
                logger.error(f"Database error during DROP SCHEMA: {pe}")
                raise

        # Check if the schema already exists
        cur.execute(
            sql.SQL("SELECT EXISTS (SELECT 1 FROM pg_namespace WHERE nspname = %s)"),
            [schema_hash_hex],
        )
        schema_exists = cur.fetchone()[0]

        if create_if_not_exists and not schema_exists:
            logger.info("Create new schema.")
            # SQL command to clone the database
            cur.execute(
                sql.SQL("CREATE SCHEMA {} AUTHORIZATION {}").format(
                    sql.Identifier(schema_hash_hex),
                    sql.Identifier(db_owner),
                )
            )

            logger.info("Copy tables from the public schema.")
            tables_to_copy = [
                "Acquisition",
                "Product",
                "Center_analysis",
                "Basin_Acquisition",
                "many_Acquisition_has_many_SimpleTrack",
                "SMOSAcq",
                "SARAcq",
                "SAR_analysis",
                "many_Acquisition_has_many_Acquisition",
            ]

            # Copy each table and its related objects from public schema to the new schema
            for table in tables_to_copy:
                # Copy table structure with constraints
                sql_cmd = sql.SQL(
                    """
                    CREATE TABLE {}.{} (LIKE public.{} INCLUDING DEFAULTS INCLUDING CONSTRAINTS INCLUDING INDEXES INCLUDING COMMENTS)
                """
                ).format(
                    sql.Identifier(schema_hash_hex),
                    sql.Identifier(table),
                    sql.Identifier(table),
                )
                logger.debug(f"Creating table structure: {sql_cmd}")
                cur.execute(sql_cmd)

                # Get foreign key constraints from the source table
                constraints = get_foreign_key_constraints(cur, "public", table)

                logger.info(f"Foreign key constraints for table {table}: {constraints}")

                # Recreate foreign key constraints in the new table
                recreate_foreign_keys(
                    cur, schema_hash_hex, table, constraints, tables_to_copy
                )

                # Copy triggers
                copy_triggers(cur, schema_hash_hex, table)

            logger.info(
                f"Schema '{schema_hash_hex}' created successfully from public tables."
            )
        else:
            logger.info(f"Schema '{schema_hash_hex}' already exists. Not re-creating.")

        # Close the cursor and connection
        cur.close()
        conn.close()

        status = True
    except psycopg2.Error as e:
        logger.error(f"An error occurred: {e}")
        if conn is not None:
            conn.close()
        raise
    if status:
        cmd_str = (
            f"source {condash_location} && conda activate {conda_env}/ && "
            f'export DATABASE_URL="{database_url}" && '
            f"cyclobsDBupdate.sh --no-ibtracs --no-atcf {add_update_args} "
            f"--proc-config {config_name} --proc-commit {commit} --proc-base-path {proc_base_path} "
            f"--listing-name {args.listing_name} --schema {schema_hash_hex}"
        )
        logger.info(f"Starting {p_id} plugin command execution...")
        try:
            # Execute the command
            process = subprocess.run(
                cmd_str,
                shell=True,
                executable="/bin/bash",
                check=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
                text=True,
            )
            output = process.stdout
            returncode = str(process.returncode)
        except subprocess.CalledProcessError as e:
            # This gives you the output of the failed command
            output = e.output
            returncode = str(e.returncode)
        except Exception:
            # Other errors (like no command found)
            output = traceback.format_exc()
            returncode = "1"

        os.makedirs(os.path.join(standalone_plugin_dir, p_id), exist_ok=True)
        log_file = os.path.join(standalone_plugin_dir, p_id, f"{p_id}.log")
        status_file = os.path.join(standalone_plugin_dir, p_id, f"{p_id}.status")
        # Write the output to the log file
        with open(log_file, "w") as file:
            file.write(output)
        with open(status_file, "w") as file:
            file.write(str(returncode))
        print(output)
        # Check if the command was successful
        if returncode == "0":
            logger.info(output)
            logger.info("Cyclobs_db plugin success")
            time.sleep(60)
        else:
            logger.error(output)
            logger.error("ERROR cyclobs_db plugin.")
