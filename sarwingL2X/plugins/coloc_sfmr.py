import logging
import traceback

logger = logging.getLogger(__name__)

import datetime
import shutil
import subprocess

from . import PluginArgs, check_status
import os
from sarwingL2X import l2m_ok
import glob
import pandas as pd
from urllib.parse import quote


def coloc_sfmr(args: PluginArgs):
    logger.info(f"Found plugin {args.plugin}")

    condash_location = args.config["condash_source_location"]
    post_proc_dir = args.config["post_processing_dir"]
    api_host = args.plugin_config["api_host"]
    sfmr_source = args.plugin_config["sfmr_source"]
    p_id = args.plugin_config["id"]
    path_tmp_df = args.plugin_config["path_tmp_df"]

    now_str = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    tmp_df_dir = os.path.join(path_tmp_df, f"cyclobs_track_df_{now_str}")
    os.makedirs(tmp_df_dir, exist_ok=True)

    # Retrieve list of SAR acquisitions co-located with SFMR Tracks from API
    df = pd.read_csv(
        f"{api_host}/app/api/getData?include_cols=all&track_source=sfmr_{quote(sfmr_source.upper())}&"
        f"instrument=C-Band_SAR&"
        f"commit={quote(args.commit)}&config={quote(args.config_name)}&listing={quote(args.listing_name)}"
    )
    logger.debug(f"API df: {df}")
    logger.info(f"Found {len(df.index)} lines from API.")

    # Get list of L2M processed OK
    l2m_lines = l2m_ok(args.report_path)

    # Convert those to path
    l2m_path = set([os.path.dirname(line) for line in l2m_lines])

    # For this plugin we want to use nclight_L2M 3km _gd. So we find these files
    nclight_gd_3km_dirs = [
        os.path.join(path, post_proc_dir, "nclight_L2M_gd") for path in l2m_path
    ]

    # Finding matching _gd files on disk
    files_candidates = []
    for nclight_path in nclight_gd_3km_dirs:
        nc_file = glob.glob(os.path.join(nclight_path, "*-*0003-*_gd.nc"))
        if len(nc_file) > 0:
            nc_file = nc_file[0]
            output_dir = os.path.join(os.path.dirname(nc_file), "..", p_id)
            status_glob = "*"
            if not check_status(output_dir, status_glob):
                files_candidates.append(nc_file)

    logger.info(f"Found {len(files_candidates)} _gd files.")

    # The pluginWrapper takes care of writing logs and status to appropriate files.
    absolute_path_wrapper = shutil.which("pluginWrapper.sh")
    coloc_script = "sfmr_create_coloc_file.py"

    # Add file_id column to df
    # Here, instead of just using the date (split and selecting [4:6]), I use the full file start ([0:6])
    # to prevent cases where two missions make an acquisition on exactly the same timestamp.
    # The end of the file is removed because it contains resolution and format (_gd, _llg_gd...) which
    # will make the matching fail later (files on API are/should be _ll_gd 3km)
    df["file_id"] = df["data_url"].apply(
        lambda x: "-".join(
            os.path.basename(x).split("-")[0:6]
        )  # s1a-ew-owi-cm-20241108t101553-20241108t101657
    )

    # Loop over files and find matching track on dataframe from API
    for nc_file in files_candidates:
        # Get date from filename
        file_id = "-".join(
            os.path.basename(nc_file).split("-")[0:6]
        )  # s1a-ew-owi-cm-20241108t101553-20241108t101657
        # Get matching row in df (match file found on disk with file from API)
        match_df = df[df["file_id"] == file_id]
        logger.debug(f"File {nc_file}, file_id {file_id}, match {match_df}")
        if len(match_df.index) > 0:
            sid = match_df.iloc[0]["sid"]
            mission_short = match_df.iloc[0]["mission_short"]
            if "sfmr" in sid.lower():
                logger.warning(
                    "SFMR found in sid field, skipping. This probably means that the SFMR couldn't be "
                    f"colocated with an atcf track. sid field : {sid}"
                )
                # If sfmr is contained in the sid row, it means that no atcf track has been linked to
                # an SFMR track by the database, so we skip coloc (because without link, the sid row contains
                # the sfmr file path). Read documentation about cyclobs database or check database model
                #  if you don't understand why can "sfmr" end up in the sid field.
                continue
            # Get the matching SFMR track file
            sfmr_file = match_df.iloc[0]["track_file"]

            # Request the whole SFMR track from API to write it to disk,
            # to make it available to datarmor compute node who is offline
            # and can't access the API.
            # Currently this is not really necessary because the plugin isn't run on datarmor
            # as it runs quite fast (optimized code and not a lot of colocations)
            track_req = (
                f"{api_host}/app/api/track?include_cols=all&source=atcf&freq=1&sid={quote(sid)}&"
                f"commit={quote(args.commit)}&config={quote(args.config_name)}&listing={quote(args.listing_name)}"
            )
            track_df = pd.read_csv(track_req)
            logger.debug(f"Track df for sid {sid} : {track_df}")
            filename_df_file = f"track_df_{sid}.csv"
            track_df_filepath = os.path.join(tmp_df_dir, filename_df_file)
            track_df.to_csv(track_df_filepath, index=False)

            # ncfile_gd = nc_file.replace("_ll_gd.nc", "_gd.nc")  # Use _gd file
            out_path = os.path.join(os.path.dirname(nc_file), f"../{p_id}")
            cmd_str = (
                f"source {condash_location} && conda activate {args.custom_conda_env}/ && "
                f"{absolute_path_wrapper} {coloc_script} {p_id} "
                f"{nc_file} ../{p_id} {p_id} -t {track_df_filepath} -s {sfmr_file} -a {nc_file} -c {sid} "
                f"-m {mission_short} -o {out_path}"
            )
            logger.info(f"Command : {cmd_str}")
            try:
                process = subprocess.run(
                    cmd_str,
                    shell=True,
                    executable="/bin/bash",
                    check=True,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    text=True,
                )
                output = process.stdout
                returncode = str(process.returncode)
            except subprocess.CalledProcessError as e:
                # This gives you the output of the failed command
                output = e.output
                returncode = str(e.returncode)
            except Exception:
                # Other errors (like no command found)
                output = traceback.format_exc()
                returncode = "1"

            if returncode == 0:
                logger.info(output)
            else:
                logger.error(output)

    logger.info(f"{args.plugin} finished.")
