#!/usr/bin/env python3

"""
Module for generating SARWing L2X reports.
"""

import logging

# Organize imports according to PEP 8 guidelines
import argparse
import os
import sys
import datetime
import glob
import csv
import collections
import locale
import math
import io
import base64
import netCDF4
import yaml
import jinja2
import pandas as pd
import matplotlib.pyplot as plt
from pkg_resources import resource_filename
from shapely.wkt import loads
from shapely import geometry
from matplotlib import patches
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import urllib.parse

from sarwingL2X.constants import *
import pathurl
from pathurl import toUrl, toPath
import re
import owi
import numpy as np


class SilentUndefined(jinja2.Undefined):
    """
    Jinja2 undefined class that returns an empty string instead of raising an error.
    """

    def _fail_with_undefined_error(self, *args, **kwargs):
        return ""

    __add__ = __radd__ = __mul__ = __rmul__ = __div__ = __rdiv__ = __truediv__ = (
        __rtruediv__
    ) = __floordiv__ = __rfloordiv__ = __mod__ = __rmod__ = __pos__ = __neg__ = (
        __call__
    ) = __getitem__ = __lt__ = __le__ = __gt__ = __ge__ = __int__ = __float__ = (
        __complex__
    ) = __pow__ = __rpow__ = _fail_with_undefined_error


# Remove the module-level logging configuration
# logging.basicConfig(level=logging.INFO)
hiDpi = 600
loDpi = 100


def time_str_from_file(file_path):
    """
    Return the last modified time of a file as a formatted string.
    """
    last_modified_time = os.path.getmtime(file_path)
    last_modified_date = datetime.datetime.fromtimestamp(last_modified_time)
    return last_modified_date.strftime("%Y-%m-%d %H:%M:%S")


def time_ago_file(file_path):
    """
    Calculate and return how long ago a file was last modified.
    """
    last_modified_time = os.path.getmtime(file_path)
    last_modified_date = datetime.datetime.fromtimestamp(last_modified_time)
    current_time = datetime.datetime.now()
    # Calculate time difference
    time_difference = current_time - last_modified_date

    # Break down the time difference
    days = time_difference.days
    hours, remainder = divmod(time_difference.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)

    # Format the "X time ago" string
    if days > 0:
        time_ago = f"{days} days ago"
    elif hours > 0:
        time_ago = f"{hours} hours ago"
    elif minutes > 0:
        time_ago = f"{minutes} minutes ago"
    else:
        time_ago = "Just now"
    logger.info(f"File '{file_path}' was last modified {time_ago}.")
    return time_ago


def check_task_status(task_dir):
    """
    Check the status of tasks in the given directory.
    """
    # Find if several or one status file in the task_dir.
    # If one status file, and status == 0, return true
    # if several status files, and all are 0, return true
    # if several status files, and some are not 0
    # ideal would be that each status is associated to a netcdf
    # but for sar_coloc it's currently not the case
    # So for now, we'll check if a least one status is 0 and if it is the case, return True
    # List all files in the directory
    if not os.path.exists(task_dir):
        return False
    files = os.listdir(task_dir)

    status_files = [f for f in files if f.endswith(".status")]

    # No status files found
    if not status_files:
        return False

    # Read and evaluate the status from each file
    all_zero = True
    any_zero = False
    for status_file in status_files:
        with open(os.path.join(task_dir, status_file), "r") as file:
            try:
                status = int(file.read().strip())
                if status != 0:
                    all_zero = False
                else:
                    any_zero = True
            except ValueError:
                logger.warning(
                    f"Status file {os.path.join(task_dir, status_file)} does not contain an integer."
                )
                return False

    # Determine the overall status
    if len(status_files) == 1:
        return all_zero
    else:
        return any_zero


def write_task_status(tasks_status, out_dir, source_file_type):
    """
    Write task statuses to output files.
    """
    for task, data_list in tasks_status.items():
        if len(data_list["ok"]) > 0:
            with open(
                os.path.join(out_dir, f"{data_list['outfile_ok']}.txt"), "w"
            ) as f:
                f.writelines("\n".join(data_list["ok"]))
            ok_df = pd.DataFrame(
                {
                    f"{source_file_type} path": data_list["ok_csv"][0],
                    "File": data_list["ok_csv"][1],
                    "Comments": data_list["ok_csv"][2],
                }
            )
            ok_df.to_csv(
                os.path.join(out_dir, f"{data_list['outfile_ok']}.csv"), index=False
            )

        if len(data_list["error"]) > 0:
            with open(
                os.path.join(out_dir, f"{data_list['outfile_err']}.txt"), "w"
            ) as f:
                f.writelines("\n".join(data_list["error"]))

            err_df = pd.DataFrame(
                {
                    f"{source_file_type} path": data_list["error_csv"][0],
                    "Directory": data_list["error_csv"][1],
                    "Comments": data_list["error_csv"][2],
                }
            )
            err_df.to_csv(
                os.path.join(out_dir, f"{data_list['outfile_err']}.csv"), index=False
            )
    logger.info(f"Task statuses written to '{out_dir}'.")


def build_plugin_report_files(
    report_dir, config_file, source_file_type, exclude_tasks=["QL", "L2MQL", "L2M"]
):
    """
    Build report files for plugins.
    """
    with open(config_file, "r") as stream:
        try:
            config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
            sys.exit(1)

    if source_file_type == "L2M":
        source_report = os.path.join(report_dir, MULTI_RES_CSV)
        drop_dup_label = "L2X file"
    elif source_file_type == "L2C":
        source_report = os.path.join(report_dir, CONCAT_CSV)
        drop_dup_label = "L2C file"

    post_proc_dir = config["post_processing_dir"]

    try:
        # Get post_processing tasks
        post_tasks = [
            s
            for s in [
                line.rstrip("\n")
                for line in open(os.path.join(report_dir, post_proc_dir)).readlines()
            ]
        ]
        # postSubTasks=[item for sublist in postSubTasks for item in sublist]
    except Exception as e:
        logger.warning(e)
        post_tasks = [""]

    # exclude some tasks
    post_tasks = [
        t for t in post_tasks if not any(t.startswith(s) for s in exclude_tasks)
    ]
    print(post_tasks)

    # Will be
    # { "task_name": {"ok_csv": [], "ok": [], "error_csv": [], "error": []} }
    tasks_status = {}
    outpath = report_dir
    m_df = pd.read_csv(source_report)
    m_df = m_df.drop_duplicates(subset=[drop_dup_label])
    m_df[f"{source_file_type} path"] = m_df[f"{source_file_type} file"].apply(
        lambda x: os.path.dirname(x)
    )
    for task in post_tasks:
        tasks_status[task] = {
            "outfile_ok": f"{task}_processok",
            "outfile_err": f"{task}_error",
            "ok": [],
            "ok_csv": ([], [], []),
            "error": [],
            "error_csv": ([], [], []),
        }

    for index, row in m_df.iterrows():
        for task in post_tasks:
            # find task dir for file
            task_path = os.path.join(
                row[f"{source_file_type} path"], post_proc_dir, task
            )

            # check task status for file
            status = check_task_status(task_path)
            if status:
                # find nc files
                nc_files = glob.glob(os.path.join(task_path, "*.nc"))
                for n_file in nc_files:
                    tasks_status[task]["ok"].append(n_file)
                    tasks_status[task]["ok_csv"][0].append(
                        row[f"{source_file_type} path"]
                    )
                    tasks_status[task]["ok_csv"][1].append(n_file)
                    tasks_status[task]["ok_csv"][2].append(row["Comments"])
            else:
                tasks_status[task]["error"].append(task_path)
                tasks_status[task]["error_csv"][0].append(
                    row[f"{source_file_type} path"]
                )
                tasks_status[task]["error_csv"][1].append(task_path)
                tasks_status[task]["error_csv"][2].append(row["Comments"])

    write_task_status(tasks_status, outpath, source_file_type=source_file_type)
    logger.info(f"Plugin report files generated for '{source_file_type}'.")


def buildReport(reportDir, config_file):
    """
    Build the SARWing L2X report.
    """
    logger.info("Starting to build report.")
    reportDir = reportDir.rstrip("/")
    with open(config_file, "r") as stream:
        try:
            config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
            sys.exit(1)

    post_proc_dir = config["post_processing_dir"]
    proc_commit = reportDir.split("/")[config["commit_index"]]
    proc_config = reportDir.split("/")[config["config_index"]]
    try:
        listing_name = open("%s/../listingName" % reportDir).readlines()[0]
    except Exception as e:
        logger.warning(e)
        listing_name = "Not found"

    reportDir = reportDir.rstrip("/")
    if not os.path.exists(reportDir):
        raise IOError("%s doesn't exist (%s)" % (reportDir, sys.argv[1]))

    logger.info("using %s as reportDir" % reportDir)
    pathurl.default_relative = reportDir

    logFileHandler = logging.FileHandler(reportDir + "/swReport.log", mode="w")
    logFileHandler.setLevel(logger.level)  # Set handler level based on logger
    logger.addHandler(logFileHandler)

    outConcatDir = os.path.join(reportDir, CONCAT_CSV)
    multiResReport = os.path.join(reportDir, MULTI_RES_CSV)
    outDirTxt = os.path.join(reportDir, OUTDIR_TXT)
    outUrlTxt = os.path.join(reportDir, OUTURL_TXT)
    owiListComments = os.path.join(reportDir, OWILISTCOMMENTS_TXT)

    reportArray = []
    # Opening concat report
    with open(outConcatDir, "r") as csvReport:
        reportDict = csv.DictReader(csvReport)
        for row in reportDict:
            reportArray.append(row)

    infoDict = {}
    infoDict["inputFiles"] = {}

    infoDict["proc_time_ago"] = time_str_from_file(outConcatDir)
    listing_file = os.path.join(
        reportDir, "../../../../../../", "listings", f"{os.path.basename(listing_name)}"
    )
    infoDict["listing_time_ago"] = time_str_from_file(
        listing_file.rstrip().replace("\n", "")
    )

    infoDict["inputFiles"][CONCAT_CSV] = {
        "count": len(reportArray),
        "description": "CSV listing with input L2, " "output L2C and cyclone info.",
    }

    try:
        infoDict["inputFiles"][MULTI_RES_CSV] = {
            "count": len(open(multiResReport, "r").readlines()),
            "description": "CSV listing with input L2C, output L2M, "
            "resolution and cyclone info",
        }
    except Exception as e:
        logger.warning(e)
        infoDict["inputFiles"][MULTI_RES_CSV] = {
            "count": "??",
            "description": "CSV listing with input L2C, output L2M, "
            "resolution and cyclone info",
        }

    try:
        infoDict["inputFiles"][OWILISTCOMMENTS_TXT] = {
            "count": len(open(owiListComments, "r").readlines()),
            "description": "TXT listing of L2 files used as input for L2C, "
            "with cyclone information",
        }
    except Exception as e:
        logger.warning(e)
        infoDict["inputFiles"][OWILISTCOMMENTS_TXT] = {
            "count": "??",
            "description": "TXT listing of L2 files used as input for L2C, "
            "with cyclone information",
        }

    plugins_infos_dict = {"inputFiles": {}}
    plugin_display_listing = [
        {
            "name": "coloc_smap",
            "description": "listing of successful SAR-SMAP colocs",
        },
        {
            "name": "coloc_smos",
            "description": "listing of successful SAR-SMOS colocs",
        },
        {
            "name": "coloc_sar",
            "description": "listing of successful SAR-SAR colocs",
        },
        {
            "name": "coloc_hy2",
            "description": "listing of successful SAR-HY2 colocs",
        },
        {
            "name": "sfmr_nesdis",
            "description": "listing of successful SAR-SFMR nesdis colocs",
        },
        {
            "name": "sfmr_hrd",
            "description": "listing of successful SAR-SFMR hrd colocs",
        },
        {"name": "tcva", "description": "listing of successful TCVA generation"},
    ]
    for display_infos in plugin_display_listing:
        try:
            label = f"{display_infos['name']}_processok.txt"
            plugins_infos_dict["inputFiles"][label] = {
                "count": len(open(os.path.join(reportDir, label), "r").readlines()),
                "description": ".TXT " + display_infos["description"],
            }
        except Exception as e:
            logger.warning(e)
        try:
            label = f"{display_infos['name']}_processok.csv"
            plugins_infos_dict["inputFiles"][label] = {
                "count": len(open(os.path.join(reportDir, label), "r").readlines()) - 1,
                "description": ".CSV "
                + display_infos["description"]
                + " (with cyclone infos)",
            }
        except Exception as e:
            logger.warning(e)

    try:
        infoDict["pkg_versions"] = {}
        pkg_vers_lines = open("%s/../proc_pkg_versions.txt" % reportDir).readlines()
        for line in pkg_vers_lines:
            pkg, ver = line.split(" ")
            infoDict["pkg_versions"][pkg] = ver
    except Exception as e:
        logger.warning(e)
        infoDict["pkg_versions"] = {}

    # owiPaths = []
    # owiPaths = open(outConcatDir).readlines()
    infoDict["proc_commit"] = proc_commit
    infoDict["proc_config"] = proc_config

    try:
        infoDict["inputSourceFileCount"] = len(open(owiListComments, "r").readlines())
    except Exception as e:
        logger.warning(e)
        infoDict["inputSourceFileCount"] = "??"

    infoDict["listingName"] = listing_name
    infoDict["listing_short_name"] = os.path.basename(listing_name)

    try:
        infoDict["gitbranch"] = open("%s/../gitbranch" % reportDir).readlines()[0]
    except Exception as e:
        logger.warning(e)
        infoDict["gitbranch"] = "undef"

    infoDict["otherListings"] = "../../../../../../reports/listings.html"
    # infoDict['otherListings'] = "undef"

    infoDict["standalone_plugins"] = []
    standalone_plugins_dir = os.path.join(reportDir, "standalone_plugins")
    standalone_subdirs = os.listdir(standalone_plugins_dir)
    standalone_plugins = [
        p
        for p in standalone_subdirs
        if os.path.isdir(os.path.join(standalone_plugins_dir, p))
    ]

    # Add browse cyclobs for this listing
    if "cyclobs_db_acq" in standalone_subdirs:
        infoDict["browse_cyclobs"] = (
            f"https://cyclobs.ifremer.fr/app/tropical?listing={urllib.parse.quote(os.path.basename(listing_name).replace('.txt', ''))}&commit={urllib.parse.quote(proc_commit)}&"
            f"config={urllib.parse.quote(proc_config)}"
        )
    else:
        infoDict["browse_cyclobs"] = False

    # Standalone plugin section prepare
    for subdir in standalone_plugins:
        print("s", subdir)
        plugin_path = os.path.join(standalone_plugins_dir, subdir)
        index_path = os.path.join(plugin_path, "index.html")
        standalone_dict = {
            "name": subdir,
            "status": "1",
            "link": (
                toUrl(index_path, schema="dmz")
                if os.path.exists(index_path)
                else toUrl(plugin_path, schema="dmz")
            ),
            "label": "Data/plots",
        }
        status_path = os.path.join(plugin_path, f"{subdir}.status")
        try:
            with open(status_path) as status_file:
                p_status = status_file.readline().strip()
                standalone_dict["status"] = p_status
        except:
            logger.warning(f"No status file for standalone plugin: {subdir}")
        version_path = os.path.join(plugin_path, f"{subdir}.version")
        try:
            with open(version_path) as version_file:
                p_version = version_file.readline().strip()
                standalone_dict["label"] = p_version
        except:
            logger.warning(f"No version file for standalone plugin: {subdir}")
        if standalone_dict["status"] != "0":
            standalone_dict["label"] = "logs"
            standalone_dict["link"] = toUrl(
                os.path.join(plugin_path, f"{subdir}.log"), schema="dmz"
            )
        infoDict["standalone_plugins"].append(standalone_dict)

    mainSubTasks = ["Concat"]

    try:
        # Get post_processing tasks
        postSubTasks = [
            s
            for s in [
                line.rstrip("\n")
                for line in open(os.path.join(reportDir, post_proc_dir)).readlines()
            ]
        ]
        # postSubTasks=[item for sublist in postSubTasks for item in sublist]
    except Exception as e:
        logger.warning(e)
        postSubTasks = [""]

    resolutions = []
    concatPathToL2MPath = {}
    L2MMode = False
    if any(re.match("L2M(\s\d+)+", s) for s in postSubTasks):
        matching = [s for s in postSubTasks if re.match("L2M(\s\d+)+", s)]
        postSubTasks[postSubTasks.index(matching[0])] = "L2M"
        resolutions = matching[0].split(" ")[
            1:
        ]  # Warning here, matchingsplit can only have 1 item...
        infoDict["resCount"] = len(resolutions)
        L2MMode = True
        if os.path.isfile(multiResReport):
            with open(multiResReport, "r") as multiResCsv:
                reportDict = csv.DictReader(multiResCsv)
                for row in reportDict:
                    concatPath = row["L2X file"]
                    res = row["Resolution"]
                    if not concatPath in concatPathToL2MPath:
                        concatPathToL2MPath[concatPath] = {}
                    concatPathToL2MPath[concatPath][res] = row["L2M file"]

    reduceTasks = []

    # Is ["Concat"] + other post_processing tasks
    subTasks = mainSubTasks + postSubTasks

    owiDict = {}
    owiDict["missings"] = []
    owiDict["errors"] = []
    owiDict["notProcessed"] = []
    owiDict["processOk"] = []
    owiDict["runnings"] = []
    owiDict["outUrl"] = []
    owiDict["outDir"] = []
    status_description = {
        "Concat": {
            "processOk": "Listing of L2C file path correctly processed",
            "errors": "Listing of L2C files in error",
            "notProcessed": "Listing of L2C files not yet processed",
            "running": "Listing of L2C files currently being processed",
            "outUrl": "Listing of L2C files URL correctly processed",
            "outDir": "Listing of L2C directories correctly processed",
        },
        "L2M": {
            "processOk": "Listing of L2M file path correctly processed",
            "errors": "Listing of L2M files in error",
            "notProcessed": "Listing of L2M files not yet processed",
            "running": "Listing of L2M files currently being processed",
            "outUrl": "Listing of L2M files URL correctly processed",
            "outDir": "Listing of L2M directories correctly processed",
        },
    }
    commentsDict = {}
    run_status = {}
    l2mStatus = {}

    L2MDict = collections.OrderedDict({})
    # L2MDict['missings']=[]
    # L2MDict['errors']=[]
    # L2MDict['notProcessed']=[]
    L2MDict["processOk"] = []
    # L2MDict['runnings']=[]
    L2MDict["outUrl"] = []
    L2MDict["outDir"] = []

    owiPaths = []
    # sort dsc by L2C date
    reportArray = sorted(
        reportArray, key=lambda i: os.path.basename(i["L2C file"]).split("-")[5]
    )
    reportArray.reverse()
    # Loop through concat report entries
    for row in reportArray:
        owiPath = row["L2C file"]
        outDir = os.path.dirname(owiPath)

        # This if statement is to avoid to put loads of "ifs" when we are not in L2M mode
        # We fill the variable with owiPath to avoir KeyError
        if owiPath not in concatPathToL2MPath:
            concatPathToL2MPath[owiPath] = {}

        sourceFiles = row["L2 files"]

        comment = ""
        l1_version = None

        run_status[owiPath] = collections.OrderedDict()

        run_status[owiPath]["shortName"] = os.path.basename(owiPath)
        run_status[owiPath]["outUrl"] = toUrl(owiPath, schema=None, relative=None)
        run_status[owiPath]["l1_ver"] = l1_version
        run_status[owiPath]["subTasks"] = {}
        run_status[owiPath]["comment"] = row["Comments"]

        sourceSplit = sourceFiles.split(" ")
        for i in range(len(sourceSplit)):
            sourceSplit[i] = toUrl(
                os.path.dirname(sourceSplit[i]) + f"/{post_proc_dir}/QL/index.html"
            )
        run_status[owiPath]["sourceFiles"] = sourceSplit

        processed = False
        error = False
        running = False
        # For each concat file, we've got a list of post_processing tasks to check
        for subTask in subTasks:
            if subTask in postSubTasks:
                # For L2M, the data is in a different path tree
                if "L2M" == subTask or "L2MQL" == subTask:
                    taskDir = outDir.replace("L2C", "L2M")
                elif "L2C" in subTask or subTask == "QL" or subTask == "l2c_plots":
                    taskDir = os.path.join(outDir, post_proc_dir, subTask)
                else:  # Suppose that all subtasks happen in L2M dir tree
                    taskDir = outDir.replace("L2C", "L2M")
                    taskDir = os.path.join(taskDir, post_proc_dir, subTask)
            else:
                taskDir = outDir

            try:
                run_status[owiPath]["subTasks"][subTask] = collections.OrderedDict({})

                # These are paths to the current post_processing task status, log, version
                statusfile = "%s/%s.status" % (taskDir, subTask)
                logfile = "%s/%s.log" % (taskDir, subTask)
                # countfiles="%s/.sarL2Processor/%s.countfiles" % (taskDir, subTask)
                versionfile = "%s/%s.version" % (taskDir, subTask)

                ###L2M
                # L2M has a special treatment because it has subreports...
                if "L2M" == subTask or "L2MQL" == subTask:
                    if "L2MQL" == subTask:
                        run_status[owiPath]["resReportPath"] = toUrl(
                            "%s/%s/multiResSummary.html" % (taskDir, post_proc_dir)
                        )
                        run_status[owiPath]["resReportLabel"] = "Summary"
                        L2MQL_status = ""
                    for res in resolutions:
                        l2mTaskDir = taskDir
                        logfile = "%s/%s%s.log" % (l2mTaskDir, subTask, res)
                        statusfile = "%s/%s%s.status" % (l2mTaskDir, subTask, res)
                        versionfile = ""

                        # l2mStatus[concatPathToL2MPath[owiPath][res]]['outUrl']=toUrl(owiPath)
                        # l2mStatus[concatPathToL2MPath[owiPath][res]]['comment']=row['Comments']
                        if "L2MQL" == subTask:
                            l2mTaskDir = "%s/%s/%s%s" % (
                                taskDir,
                                post_proc_dir,
                                subTask,
                                res,
                            )
                            logfile = "%s/%s.log" % (l2mTaskDir, subTask)
                            statusfile = "%s/%s.status" % (l2mTaskDir, subTask)

                        # Set per-resolution status in run_status
                        running, error, processed = checkL2MStatus(
                            statusfile,
                            owiPath,
                            logfile,
                            versionfile,
                            subTask,
                            postSubTasks,
                            outDir,
                            l2mTaskDir,
                            run_status,
                            owiPaths,
                            running,
                            error,
                            processed,
                            res,
                        )
                        if "L2MQL" == subTask:
                            if error:
                                L2MQL_status = "1"
                            elif processed:
                                L2MQL_status = "0"
                    # We only display one column for all the L2MQL, which is a link to the summary with all L2MQL in it
                    # If one L2MQL failed, we set the whole box to error
                    if "L2MQL" == subTask:
                        run_status[owiPath]["subTasks"][subTask][
                            "status"
                        ] = L2MQL_status
                ###end L2M
                else:
                    logger.debug(f"Checking status for {statusfile}")
                    running, error, processed = checkStatus(
                        statusfile,
                        owiPath,
                        logfile,
                        versionfile,
                        subTask,
                        postSubTasks,
                        outDir,
                        taskDir,
                        run_status,
                        owiPaths,
                        running,
                        error,
                        processed,
                    )

            except Exception as e:
                logger.warning(str(e))
                continue

        commentsDict[owiPath] = run_status[owiPath]["comment"]
        for res in concatPathToL2MPath[owiPath]:
            commentsDict[concatPathToL2MPath[owiPath][res]] = run_status[owiPath][
                "comment"
            ]

        if running:
            owiDict["runnings"].append(owiPath)
        elif error:
            owiDict["errors"].append(owiPath)
        elif not processed:
            owiDict["notProcessed"].append(owiPath)
        else:
            owiDict["processOk"].append(owiPath)
            owiDict["outUrl"].append(run_status[owiPath]["outUrl"])
            owiDict["outDir"].append(outDir)
            commentsDict[outDir] = run_status[owiPath]["comment"]
            commentsDict[run_status[owiPath]["outUrl"]] = run_status[owiPath]["comment"]

            l2mPaths = list(concatPathToL2MPath[owiPath].values())
            L2MDict["processOk"].append(concatPathToL2MPath[owiPath])
            urlDict = {}
            outDirDict = {}
            for res in concatPathToL2MPath[owiPath]:
                url = toUrl(
                    concatPathToL2MPath[owiPath][res], schema=None, relative=None
                )
                dirPath = os.path.dirname(concatPathToL2MPath[owiPath][res])
                urlDict[res] = url
                outDirDict[res] = dirPath
                commentsDict[url] = run_status[owiPath]["comment"]
                commentsDict[dirPath] = run_status[owiPath]["comment"]

            L2MDict["outUrl"].append(urlDict)
            L2MDict["outDir"].append(outDirDict)

    fieldnames = ["L2C path", "Comments"]
    for status in owiDict:
        with open("%s/Concat%s.csv" % (reportDir, status), "w+") as f:
            writer = csv.DictWriter(f, fieldnames=fieldnames)
            writer.writeheader()
            for p in owiDict[status]:
                writer.writerow({"Comments": commentsDict[p], "L2C path": p})
        with open("%s/Concat%s.txt" % (reportDir, status), "w+") as f:
            f.writelines("%s\n" % l for l in owiDict[status])

    if L2MMode:
        fieldnames = ["L2M path", "Resolution", "Comments"]

        for status in L2MDict:
            with open("%s/MultiRes%s.csv" % (reportDir, status), "w+") as f:
                writer = csv.DictWriter(f, fieldnames=fieldnames)
                writer.writeheader()
                for resDict in L2MDict[status]:
                    for res in resDict:
                        writer.writerow(
                            {
                                "Comments": commentsDict[resDict[res]],
                                "Resolution": res,
                                "L2M path": resDict[res],
                            }
                        )
            with open("%s/MultiRes%s.txt" % (reportDir, status), "w+") as f:
                for resDict in L2MDict[status]:
                    f.writelines("%s\n" % l for l in list(resDict.values()))

    build_plugin_report_files(reportDir, config_file, source_file_type="L2M")
    build_plugin_report_files(reportDir, config_file, source_file_type="L2C")

    imgDict = {}
    if owiPaths:
        createWorldMap(reportDir, imgDict, owiPaths)
        dates = getOwiDates(owiPaths)
        minDate = min(dates)
        maxDate = max(dates)
        createBarChart(dates, reportDir, imgDict)

        infoDict["acquiTimeRange"] = "%s - %s" % (
            minDate.strftime("%B %Y"),
            maxDate.strftime("%B %Y"),
        )

        if os.path.exists(
            os.path.join(
                reportDir,
                STANDALONE_PLUGINS_SUBDIR,
                "l2c_stats_plots",
                "l2c_wind_distribution.png",
            )
        ):
            imgDict["distribution_plot"] = os.path.join(
                reportDir,
                STANDALONE_PLUGINS_SUBDIR,
                "l2c_stats_plots",
                "l2c_wind_distribution.png",
            )
    else:
        logger.warn("There is no 'ok processed' owi path")

    html_file_path = resource_filename("sarwingL2X", "templates/report.html")

    with open(html_file_path, "r") as file:
        html_content = file.read()

    t = jinja2.Template(html_content, undefined=SilentUndefined)

    qjob = None
    html = t.render(
        subTasks=subTasks,
        run_status=run_status,
        owiDict=owiDict,
        L2MDict=L2MDict,
        L2MMode=L2MMode,
        qjob=qjob,
        infoDict=infoDict,
        plugins_infos_dict=plugins_infos_dict,
        imgDict=imgDict,
        reduceTasks=reduceTasks,
        status_description=status_description,
    )
    reportFname = "%s/index.html" % (reportDir)
    # reportFname="%s/concatReport.html" % (".")
    reportFid = open(reportFname, "w")
    reportFid.write(html)
    reportFid.close()
    logger.info("Report built successfully.")
    logger.info("report : %s", toUrl(reportFname, relative=None, schema="dmz"))


def checkStatus(
    statusfile,
    owiPath,
    logfile,
    versionfile,
    subTask,
    postSubTasks,
    outDir,
    taskDir,
    run_status,
    owiPaths,
    running,
    error,
    processed,
):
    """
    Check the status of a subtask and update the run status accordingly.
    """
    run_status[owiPath]["subTasks"][subTask]["label"] = " "
    status_found = False
    multi_status = False
    str_status = None
    if os.path.exists(statusfile):
        status_found = True
        str_status = open(statusfile).read().replace("\n", "")
    else:
        # If there is not status file with the expected name, we check all status file in the dir and if at least
        # one as bad status, we mark the whole subtask with the worst status
        status_files = glob.glob(os.path.join(taskDir, "*.status"))
        if len(status_files) > 0:
            status_found = True
            multi_status = True  # It is expected that if multi_status is found, it is the same for log and versions
        has_warning = False
        has_ok = False
        has_error = False
        for s_f in status_files:
            with open(s_f) as tmp_status:
                t_status = tmp_status.read().replace("\n", "")

                if t_status == "1":
                    str_status = t_status
                    has_error = True
                    break
                elif t_status != "0" and str_status != "1":
                    has_warning = True
                else:
                    has_ok = True
        if not has_error and has_ok and has_warning:
            str_status = "3"
        elif not has_error and has_ok:
            str_status = "0"

    if status_found:
        if "Concat" in subTask:
            processed = True

        str_version = None

        if multi_status:
            run_status[owiPath]["subTasks"][subTask]["logfile"] = toUrl(taskDir)
            run_status[owiPath]["subTasks"][subTask]["link"] = toUrl(taskDir)
            run_status[owiPath]["subTasks"][subTask]["label"] = "logs"
            versions_filepath = glob.glob(os.path.join(taskDir, "*.version"))
            if len(versions_filepath) > 0:  # Take the first version file we found
                str_version = open(versions_filepath[0]).read().replace("\n", "")
        else:
            if os.path.exists(logfile):
                run_status[owiPath]["subTasks"][subTask]["logfile"] = toUrl(logfile)
                run_status[owiPath]["subTasks"][subTask]["link"] = toUrl(logfile)
                run_status[owiPath]["subTasks"][subTask]["label"] = "logs"

            if os.path.exists(versionfile):
                str_version = open(versionfile).read().replace("\n", "")

        run_status[owiPath]["subTasks"][subTask]["status"] = str_status
        if str_status == "1":
            # Error : link log file
            if "Concat" in subTask:
                error = True
            run_status[owiPath]["subTasks"][subTask]["link"] = run_status[owiPath][
                "subTasks"
            ][subTask]["logfile"]
        elif str_status == "0":
            if "Concat" in subTask:
                owiPaths.append(owiPath)
                # print outDir.split(outDir.split("/")[outDir.split("/").index("processings")+3])[1]
                # run_status[owiPath]['subTasks'][subTask]['link']="%s/../../../%s" % (reportDir, outDir.split(outDir.split("/")[outDir.split("/").index("processings")+3])[3])
                run_status[owiPath]["subTasks"][subTask]["link"] = toUrl(outDir)
                run_status[owiPath]["subTasks"][subTask]["label"] = os.path.basename(
                    owiPath
                )
            elif subTask in postSubTasks:
                # link to taskDir (TODO: if index.html, so link to it)
                if os.path.exists("%s/index.html" % taskDir):
                    linkPath = "%s/index.html" % taskDir
                    if "QL" in subTask:
                        try:
                            qlFile = glob.glob("%s/*owiWindSpeed*.png" % taskDir)[0]
                            run_status[owiPath]["subTasks"][subTask]["QL"] = toUrl(
                                qlFile
                            )
                        except:
                            try:
                                qlFile = glob.glob(
                                    "%s/*owiPreProcessing-ND_2.png" % taskDir
                                )[0]
                                run_status[owiPath]["subTasks"][subTask]["QL"] = toUrl(
                                    qlFile
                                )
                            except:
                                logger.debug(
                                    "no owi QL files found : %s/*owiWindSpeed_Tab_dualpol_2steps*.png or %s/*owiPreProcessing-ND_2.png\n"
                                    % (taskDir, taskDir)
                                )

                else:
                    linkPath = taskDir
                run_status[owiPath]["subTasks"][subTask]["link"] = toUrl(linkPath)
                if str_version:
                    run_status[owiPath]["subTasks"][subTask]["label"] = str_version
                else:
                    run_status[owiPath]["subTasks"][subTask]["label"] = subTask
            else:
                run_status[owiPath]["subTasks"][subTask]["link"] = run_status[owiPath][
                    "subTasks"
                ][subTask]["logfile"]
        else:  # Other status than 0 or 1
            run_status[owiPath]["subTasks"][subTask]["link"] = run_status[owiPath][
                "subTasks"
            ][subTask]["logfile"]
    else:
        logger.debug("No status file found for %s" % taskDir)

    logger.debug(f"Status for '{subTask}' in '{owiPath}': {str_status}")
    return running, error, processed


def checkL2MStatus(
    statusfile,
    owiPath,
    logfile,
    versionfile,
    subTask,
    postSubTasks,
    outDir,
    taskDir,
    run_status,
    owiPaths,
    running,
    error,
    processed,
    resolution,
):
    run_status[owiPath]["subTasks"][subTask][resolution] = {}
    run_status[owiPath]["subTasks"][subTask][resolution]["label"] = " "
    if os.path.exists(logfile):
        run_status[owiPath]["subTasks"][subTask][resolution]["logfile"] = toUrl(logfile)
        run_status[owiPath]["subTasks"][subTask][resolution]["link"] = toUrl(logfile)
        run_status[owiPath]["subTasks"][subTask][resolution]["label"] = "logs"
    if os.path.exists(statusfile):
        processed = True

        str_version = None
        if os.path.exists(versionfile):
            str_version = open(versionfile).read().replace("\n", "")

        str_status = open(statusfile).read().replace("\n", "")
        run_status[owiPath]["subTasks"][subTask][resolution]["status"] = str_status
        run_status[owiPath]["subTasks"][subTask][resolution]["label"] = resolution
        if str_status != "0":
            # Error : link log file
            error = True
            run_status[owiPath]["subTasks"][subTask][resolution]["link"] = run_status[
                owiPath
            ]["subTasks"][subTask][resolution][
                "logfile"
            ]  ####
        else:
            if subTask in postSubTasks:
                if os.path.exists("%s/index.html" % taskDir):
                    linkPath = "%s/index.html" % taskDir
                else:
                    linkPath = taskDir
                run_status[owiPath]["subTasks"][subTask][resolution]["link"] = toUrl(
                    linkPath
                )

    return running, error, processed


def getFootprint(fname):
    try:
        nc = netCDF4.Dataset(fname)
    except Exception as e:
        logger.warning("skipping unreadable %s : %s" % (fname, str(e)))
        return None

    if "footprint" in nc.ncattrs():
        return nc.footprint
    else:
        return None


def createWorldMap(reportDir, imgDict, owiPaths):
    """
    Create a world map with bounding boxes from the OWI paths.
    """
    logger.info("Creating world map with bounding boxes.")
    fig, ax = plt.subplots(subplot_kw={"projection": ccrs.PlateCarree()})
    ax.add_feature(cfeature.COASTLINE)
    ax.add_feature(cfeature.BORDERS, linestyle=":")
    ax.set_global()

    owiPolys = getOwiPoly(owiPaths)

    def add_polygon(ax, polygon):
        if not isinstance(polygon, geometry.Polygon):
            polygon = geometry.Polygon(polygon)

        exterior_coords = polygon.exterior.coords
        patch = patches.Polygon(
            exterior_coords,
            closed=True,
            edgecolor="blue",
            facecolor="cyan",
            alpha=0.5,
            transform=ccrs.PlateCarree(),
        )
        ax.add_patch(patch)

        for interior in polygon.interiors:
            interior_coords = interior.coords
            patch = patches.Polygon(
                interior_coords,
                closed=True,
                edgecolor="blue",
                facecolor="red",
                alpha=0.5,
                transform=ccrs.PlateCarree(),
            )
            ax.add_patch(patch)

    for owiPoly in owiPolys:
        add_polygon(ax, owiPoly)

    worldPngPath = "%s/world_map.png" % reportDir
    plt.savefig(worldPngPath, dpi=hiDpi, bbox_inches="tight")
    ioBytes = io.BytesIO()
    plt.savefig(ioBytes, format="png", dpi=loDpi, bbox_inches="tight")
    ioBytes.seek(0)
    imgDict["world_png_small"] = (
        b"data:image/png;base64, " + base64.b64encode(ioBytes.read())
    ).decode("utf-8")
    imgDict["world_png"] = os.path.basename(worldPngPath)
    logger.info("World map created at '%s'.", worldPngPath)


def getOwiPoly(owiPaths):
    owiPolyg = []
    for owiPath in owiPaths:
        footprint = getFootprint(owiPath)
        if footprint:
            owiPolyg.append(loads(footprint))
        else:
            logger.warn("No footprint for file %s" % owiPath)

    return owiPolyg


def createBarChart(dates, reportDir, imgDict):
    """
    Create a bar chart showing the distribution of dates.
    """
    logger.info("Creating bar chart of date distribution.")
    locale.setlocale(locale.LC_ALL, "en_US.utf8")
    fig = plt.figure(figsize=(20, 3))
    plt.grid()
    minDate = min(dates)
    maxDate = max(dates)

    countDateDict = {}
    delta = maxDate - minDate
    quarter = False
    if delta.days < 400:
        for year in range(minDate.year, maxDate.year + 1):
            minMonth = 1
            maxMonth = 12
            if year == minDate.year:
                minMonth = minDate.month
            if year == maxDate.year:
                maxMonth = maxDate.month
            for month in range(minMonth, maxMonth + 1):
                date = datetime.date(year, month, 1)
                countDateDict[date] = 0

        for date in dates:
            dateKey = datetime.date(date.year, date.month, 1)
            countDateDict[dateKey] += 1
    else:
        quarter = True
        for year in range(minDate.year, maxDate.year + 1):
            minMonth = 1
            maxMonth = 12
            if year == minDate.year:
                minMonth = minDate.month
            if year == maxDate.year:
                maxMonth = maxDate.month
            for month in range(minMonth, maxMonth + 1):
                tri = int(math.ceil(month / 3.0))
                date = datetime.date(year, tri, 1)
                countDateDict[date] = 0

        for date in dates:
            dateKey = datetime.date(date.year, int(math.ceil(date.month / 3.0)), 1)
            countDateDict[dateKey] += 1

    ordered = collections.OrderedDict(
        sorted(list(countDateDict.items()), key=lambda t: t[0])
    )
    datesKey = list(ordered.keys())
    yearSet = []
    labels = []

    for date in datesKey:
        if quarter:
            date1 = datetime.date(date.year, date.month * 3 - 2, date.day)
            date2 = datetime.date(date.year, date.month * 3 - 1, date.day)
            date3 = datetime.date(date.year, date.month * 3, date.day)
            labels.append(
                "%s/%s/%s\n%s"
                % (
                    date1.strftime("%b"),
                    date2.strftime("%b"),
                    date3.strftime("%b"),
                    date.year,
                )
            )
        elif date.year in yearSet:
            labels.append(date.strftime("%B"))
        else:
            labels.append(date.strftime("%B\n%Y"))
            yearSet.append(date.year)

    x = np.arange(len(ordered))
    p = plt.bar(x, list(ordered.values()), 0.2)
    plt.xticks(x, labels)

    timePngPath = "%s/time_map.png" % reportDir
    plt.savefig(timePngPath, dpi=hiDpi, bbox_inches="tight")
    ioBytes = io.BytesIO()
    plt.savefig(ioBytes, format="png", dpi=loDpi, bbox_inches="tight")
    ioBytes.seek(0)
    imgDict["time_png_small"] = (
        b"data:image/png;base64, " + base64.b64encode(ioBytes.read())
    ).decode("utf-8")
    imgDict["time_png"] = os.path.basename(timePngPath)
    logger.info("Bar chart created at '%s'.", timePngPath)


def getOwiDates(owiPaths):
    dates = []
    for owiPath in owiPaths:
        dates.append(owi.getDateFromFname(owiPath))

    return dates


logger = logging.getLogger(__name__)


def main():
    """
    Entry point of the script.
    """
    parser = argparse.ArgumentParser(
        description="Generate SARWing L2X reports based on report directory and configuration file."
    )
    parser.add_argument("reportDir", type=str, help="Path to the report directory.")
    parser.add_argument(
        "config_file", type=str, help="Path to the configuration YAML file."
    )
    # Add a debug option to change the log level
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Set log level to DEBUG to see detailed logs.",
    )
    args = parser.parse_args()

    # Configure logging based on the debug option
    log_level = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(level=log_level)
    logger.setLevel(log_level)

    reportDir = toPath(args.reportDir)
    config_file = toPath(args.config_file)

    if not os.path.exists(reportDir):
        logger.error(f"Report directory '{reportDir}' does not exist.")
        sys.exit(1)

    if not os.path.exists(config_file):
        logger.error(f"Config file '{config_file}' does not exist.")
        sys.exit(1)

    buildReport(reportDir, config_file)


if __name__ == "__main__":
    main()
