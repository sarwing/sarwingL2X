import datetime
import logging
import subprocess
import uuid

logger = logging.getLogger(__name__)

import shutil
from . import PluginArgs, write_content_to_unique_file
import os
import yaml
from sarwingL2X import l2m_ok
from subprocess import Popen, PIPE
import glob
import geopandas as gpd
import pandas as pd
import sarnomencl
from shapely.geometry import Polygon, MultiPolygon


def get_l2_path_from_ocn_SAFE(ocn_SAFE):
    f_full = sarnomencl.sar2path(
        ocn_SAFE,
        skip=True,
        prefix="/home/datawork-cersat-public/cache/project/mpc-sentinel1/data/esa",
    )
    splt = f_full.split("/")
    splt[-4] = "*"
    splt_safe = splt[-1].split("_")
    splt_safe[2] = "*"
    del splt_safe[3]
    splt_safe[3] = "*"
    splt_safe[-1] = "*.SAFE"
    splt_safe[-2] = "*"
    splt_safe[-3] = "*"
    splt[-1] = "_".join(splt_safe)
    path_search = "/".join(splt).replace("/L2/", "/L1/")


def coloc_hy2(args: PluginArgs):
    post_proc_dir = args.config["post_processing_dir"]
    commit_index = int(args.config["commit_index"])
    config_index = int(args.config["config_index"])
    configs_directory = args.config["configs_directory"]
    delta_time = args.plugin_config["maximum_delta_time_min"]
    minimal_area = args.plugin_config["minimal_area_km"]
    owi_comments_txt_file = os.path.join(args.report_path, "owiList_comments.txt")
    concat_report_csv = os.path.join(args.report_path, "concatReport.csv")
    tmp_dir = args.config["tmp_script_dir"]

    parquet_file_loc = args.plugin_config["crossovers_parquet_file"]

    use_qsubarray = args.plugin_config.get("use_qsubarray", args.use_qsubarray_default)
    ssh_host = args.plugin_config.get("ssh_host", args.ssh_host_default)

    if use_qsubarray:
        qsubarray = args.plugin_config.get("qsubarray", os.getenv("QSUBARRAY", ""))
    else:
        qsubarray = ""

    if ssh_host:
        condash_location = args.config["condash_source_location_remote"]
        ssh_host_cmd = f"rsh {ssh_host} 'bash -c \""
        ssh_host_cmd_end = "\"'"
    else:
        condash_location = args.config["condash_source_location"]
        ssh_host_cmd = ""
        ssh_host_cmd_end = ""

    logger.info(f"Found plugin {args.plugin}")

    unique_filename = f"{uuid.uuid4()}.parquet"
    tmp_prq_path = os.path.join(tmp_dir, unique_filename)
    # shutil.copy(parquet_file_loc, tmp_prq_path)
    logger.info(f"Reading {owi_comments_txt_file}")
    with open(owi_comments_txt_file, "r") as owi_cmt:
        owi_lines = owi_cmt.readlines()

    concat_report_df = pd.read_csv(concat_report_csv)

    logger.info(f"Opening parquet file {parquet_file_loc}")

    # Opening the parquet file to modify "ref_granule" column content to set produced files instead.
    # Question : match with L2C or L2 files ? Match with L2C file is not hard using takeID but
    # maybe a swath concatenated with L2C file will colocate with a HY2 file, and another SAR swath
    # of the same L2C file colocate with another HY2 file.
    # It looks it raises less questions to colocate directly with L2, so that is what I'm going to
    # do.

    # destination_folder will also be edited in the parquet to point to individual coloc_hy2 post
    # processing folder.
    gdf = gpd.read_parquet(parquet_file_loc)

    # 'S1A_IW_OCN__2SDV_20200501T044820_20200501T044837_032367_03BF2C_B8D3.SAFE'
    gdf["ref_granule_date_part"] = gdf["ref_granule"].apply(
        lambda x: "_".join(x.split("_")[5:7])
    )

    # Find source SAR file from specified OCN SAFE in the .parquet file.
    # I think the easiest and fastest way to do that is to extract dates from the OCN SAFE name
    # and then find the corresponding path in the "owiList_comments.txt" which list the procuded L2 files
    # for a listing.

    matching_rows = []
    for owi_line in owi_lines:
        # /home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default
        # /sentinel-1a/L1/IW/S1A_IW_GRDH_1S/2024/147/S1A_IW_GRDH_1SSV_20240526T095854_20240526T095928
        # _054041_069204_920F.SAFE/s1a-iw-owi-xx-20240526t095854-20240526t095928-054041-
        # 069204.nc EWINIAR cat-1 eye partial -0.3 -0.3
        owi_date = "_".join(owi_line.split(" ")[0].split("/")[-2].split("_")[5:7])
        owi_date_filename = "-".join(owi_line.split(" ")[0].split("/")[-1].split("-")[4:6])
        owi_date_fname_start = datetime.datetime.strptime(owi_date_filename.split("-")[0], "%Y%m%dt%H%M%S")
        owi_date_fname_end = datetime.datetime.strptime(owi_date_filename.split("-")[1], "%Y%m%dt%H%M%S")

        # Locate row of parquet file matching current owi_line (owi_date)
        matched_rows = gdf.loc[gdf["ref_granule_date_part"] == owi_date]
        if len(matched_rows.index) == 0:
            logger.warning(f"Couldn't find matching row for {owi_line}")
            continue

        logger.info(f"Matched {len(matched_rows.index)} rows")

        # Find location of L2C dir for L2 file, using ConcatReport.csv content
        matched_concat = concat_report_df.loc[
            concat_report_df["L2 files"].str.contains(owi_date)
        ]
        if len(matched_concat.index) == 0:
            raise ValueError(
                f"Coudldn't find matching L2C in concatReport.csv for {owi_line}, extracted date:{owi_date}"
            )

        l2c_path = os.path.dirname(matched_concat.iloc[0]["L2C file"])
        # Use L2M path because report uses L2M path for plugins
        l2m_path = l2c_path.replace("L2C", "L2M")
        dest = os.path.join(l2m_path, post_proc_dir, args.plugin)
        os.makedirs(dest, exist_ok=True)
        # Set destination directory in parquet that'll be written
        logger.info(f"DEST {dest}")
        matched_row = matched_rows.iloc[0]
        matched_row["destination_folder"] = dest

        # It is necessary to replace the match time because the ones in SAFE and .nc doesn't match exaclty, so the result (without doing this and using .SAFE times) is that 
        # the coloc program fails to find the SAR files
        matched_row["ref_start"] = owi_date_fname_start
        matched_row["ref_end"] = owi_date_fname_end

        matching_rows.append(matched_row)

    if len(matching_rows) == 0:
        logger.info("Empty matching set, stopping now.")
        return

    matched_gdf = gpd.GeoDataFrame(matching_rows)
    matched_gdf = matched_gdf.set_geometry("ref_geometry")

    # Identify geometry columns
    geometry_columns = [
        col
        for col in matched_gdf.columns
        if isinstance(matched_gdf[col].iloc[0], (Polygon, MultiPolygon))
    ]
    # geometry_columns.remove("ref_geometry")

    # Convert all geometry columns to WKT
    for column in geometry_columns:
        matched_gdf[column] = gpd.GeoSeries(matched_gdf[column])

    logger.info(f"Writing temporary {tmp_prq_path}")
    logger.info(matched_gdf["destination_folder"])
    matched_gdf.to_parquet(tmp_prq_path)
    try:

        # Define config for coloc
        coloc_config_yaml = f"""paths:
    HY2:
    - '/home/datawork-cersat-public/provider/knmi/satellite/l2b/hy-2b/hscat/25km/data/%Y/%(dayOfYear)/*%Y%m%d_%H%M*.nc'
    S1:
        L2:
        - '/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/{args.commit}/{args.config_name}/sentinel-1*/L2/*/*/%Y/%(dayOfYear)/S1*%Y%m%d*/s1*-owi-*-%Y%m%dt%H%M%S*.nc'
        #- '/tmp/chain_processing/processings/{args.commit}/{args.config_name}/sentinel-1*/L2/*/*/%Y/%(dayOfYear)/S1*%Y%m%d*/s1*-owi-vv-%Y%m%dt%H%M%S*.nc'
        #- '/home/datawork-cersat-public/cache/project/sarwing/data/sentinel-1*/*/*/*/%Y/%(dayOfYear)/S1*%Y%m%d*/post_processing/nclight_L2M/s1*owi*%Y%m%d*000003*_ll_gd.nc'
        #- '/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1*/*/*/*/%Y/%(dayOfYear)/S1*%Y%m%d*/post_processing/nclight_L2M/s1*owi*%Y%m%d*0003*_ll_gd.nc'

common_var_names:
    wind_speed: wind_speed
    wind_direction: wind_direction_ecmwf
    wind_from_direction: wind_from_direction
    longitude: lon
    latitude: lat
    time: time

# Config necessary for parquet script
dataset_name_1: "S1"
dataset_name_2: "HY2"
match_time_accuracy_1: "second"
match_time_accuracy_2: "minute"
match_filename_1: false
match_filename_2: true
match_time_delta_seconds_1: 0
match_time_delta_seconds_2: 0"""

        # Write config to a file
        os.makedirs(os.path.join(configs_directory, "tmp"), exist_ok=True)
        config_path_tmp = os.path.join(
            configs_directory,
            "tmp",
            f"l2x_coloc_hy2_{args.commit}_{args.config_name}_{args.listing_name}.yaml",
        )
        with open(config_path_tmp, "w") as conf_file:
            conf_file.write(coloc_config_yaml)

        # Additionnal args from config to add to command
        add_arg_cmd = ""

        if "n_workers" in args.plugin_config:
            add_arg_cmd += f"--n-workers {args.plugin_config['n_workers']} "

        if "memory" in args.plugin_config:
            add_arg_cmd += f"--memory {args.plugin_config['memory']} "

        if "datarmor" in args.plugin_config:
            if args.plugin_config["datarmor"] is True:
                add_arg_cmd += f"--parallel-datarmor "

        cmd_str = (
            f"{ssh_host_cmd} {args.post_ssh_cmd} source {condash_location} && conda activate {args.custom_conda_env}/ && "
            f'{qsubarray} xargs -ICMD bash -c "CMD" {ssh_host_cmd_end}'
        )

        logger.info(f"Starting {args.plugin} plugin execution...")
        logger.info(f"Command: {cmd_str}")
        process = subprocess.Popen(
            cmd_str,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            executable="/bin/bash",
            shell=True,
        )
        # Coloc_from_parquet --parquet /home/datawork-cersat-public/cache/project/hr-wind/crossovers/
        # s1a__hy-2b/2023/s1a__hy-2b__20230101_20230201.parquet
        # --filter-dataset-unique "ref" --destination-folder /home1/datahome/tcevaer/coloc_hy2/
        # --delta-time 120 --minimal-area "30km2" --product-generation --config ./coloc_sat/config.yml
        # --debug
        input_stdin = (
            f"Coloc_from_parquet --parquet {tmp_prq_path} --filter-dataset-unique 'ref' "
            f"--delta-time {delta_time} --minimal-area {minimal_area} "
            f"--product-generation --config {config_path_tmp} {add_arg_cmd}"
        )
        output, _ = process.communicate(input=input_stdin.encode("utf-8"))
        output = output.decode("utf-8")
        returncode = process.returncode

        #plugin_report_dir = os.path.join(args.standalone_plugin_dir, args.plugin)
        #os.makedirs(plugin_report_dir, exist_ok=True)

        #log_file = os.path.join(plugin_report_dir, f"{args.plugin}.log")
        #status_file = os.path.join(plugin_report_dir, f"{args.plugin}.status")
        # Write the output to the log file
        # with open(log_file, "w") as file:
        #    file.write(output)
        # with open(status_file, "w") as file:
        #    file.write(str(returncode))
        print(output)
        # Check if the command was successful
        if returncode == "0":
            logger.info(output)
            logger.info(f"{args.plugin} plugin success")
        else:
            logger.error(output)
            logger.error(f"{args.plugin} failure.")
    except Exception as e:
        raise e
    finally:
        os.remove(tmp_prq_path)
