#!/usr/bin/env python

import logging
import os

if __name__ == "__main__":
    description = """Triggers L2X processings"""

    import argparse

    parser = argparse.ArgumentParser(description=description)

    parser.add_argument(
        "--debug",
        action="store_true",
        default=False,
        help="start the script in debug mode",
    )
    parser.add_argument(
        "-d",
        "--outdir",
        action="store",
        default=".",
        type=str,
        help="Base output directory (other directories will be created inside)",
    )
    parser.add_argument(
        "--report-path",
        action="store",
        default=None,
        help="Specify the path and filename where to record the created files",
    )
    parser.add_argument(
        "-f",
        "--force",
        action="store_true",
        default=False,
        help="Forces existing files to be recreated.",
    )
    parser.add_argument(
        "--config",
        action="store",
        type=str,
        required=True,
        help="Configuration for process (also passed to plugins, see config.yaml)",
    )
    parser.add_argument(
        "--plugins-only-exec",
        type=str,
        help="Plugins to execute (must be present in config file), ignoring other "
        "plugins. Comma separated list of plugin names.",
    )

    args = parser.parse_args()

    if args.report_path is None:
        log_path = "/tmp/swProcessor.log"
    else:
        log_path = os.path.join(args.report_path, "swProcessor.log")

    log_level = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(
        filename=log_path,
        level=log_level,
        format="%(asctime)s [%(levelname)s] %(message)s",
    )

    logger = logging.getLogger(__name__)

    if args.plugins_only_exec:
        plugins_to_exec = args.plugins_only_exec.split(",")
    else:
        plugins_to_exec = None

    from sarwingL2X import l2xProcessor

    l2xProcessor(
        args.config,
        args.outdir,
        args.force,
        args.report_path,
        args.debug,
        plugins_to_exec,
    )
