import logging
import subprocess

logger = logging.getLogger(__name__)

from . import PluginArgs
import os
import traceback


def coloc_stats_plots(args: PluginArgs):
    logger.info(f"Found plugin {args.plugin}")

    use_qsubarray = args.plugin_config.get("use_qsubarray", args.use_qsubarray_default)
    ssh_host = args.plugin_config.get("ssh_host", args.ssh_host_default)
    use_ssh = args.plugin_config.get("use_ssh", False)
    force = args.plugin_config.get("force", False)

    if use_qsubarray:
        qsubarray = args.plugin_config.get("qsubarray", os.getenv("QSUBARRAY", ""))
    else:
        qsubarray = ""

    if use_ssh:
        condash_location = args.config["condash_source_location_remote"]
        ssh_host_cmd = f"rsh {ssh_host} 'bash -c \""
        ssh_host_cmd_end = "\"'"
    else:
        condash_location = args.config["condash_source_location"]
        ssh_host_cmd = ""
        ssh_host_cmd_end = ""

    listing_path = os.path.join(args.report_path, "MultiResprocessOk.txt")
    plot_report_dir = os.path.join(args.standalone_plugin_dir, args.plugin)
    os.makedirs(plot_report_dir, exist_ok=True)

    # read status, do not execute if status == 0
    status_file = os.path.join(plot_report_dir, f"{args.plugin}.status")
    if not force and os.path.exists(status_file):
        with open(status_file, "r") as file:
            status = file.read().strip()
        if status == "0":
            logger.info(f"{args.plugin} already executed")
            return

    cmd_str = (
        f"{ssh_host_cmd} {args.post_ssh_cmd} source {condash_location} && conda activate {args.custom_conda_env}/ && "
        f'{qsubarray}  xargs -ICMD bash -c "CMD" {ssh_host_cmd_end}'
    )
    logger.info(f"Starting {args.plugin} plugin execution...")
    try:
        # Execute the command
        process = subprocess.Popen(
            cmd_str,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            executable="/bin/bash",
            shell=True,
            text=True,
        )
        input_stdin = f"plot_all_colocs_report -i {listing_path} -o {plot_report_dir}"
        logger.info("Command: " + input_stdin)
        output, _ = process.communicate(input=input_stdin)
        returncode = str(process.returncode)
    except subprocess.CalledProcessError as e:
        # This gives you the output of the failed command
        output = e.output
        returncode = str(e.returncode)
    except Exception:
        # Other errors (like no command found)
        output = traceback.format_exc()
        returncode = "1"

    log_file = os.path.join(plot_report_dir, f"{args.plugin}.log")
    status_file = os.path.join(plot_report_dir, f"{args.plugin}.status")
    # Write the output to the log file
    with open(log_file, "w") as file:
        file.write(output)
    with open(status_file, "w") as file:
        file.write(returncode)
    print(output)
    # Check if the command was successful
    if returncode == "0":
        logger.info(output)
        logger.info(f"{args.plugin} plugin success")
    else:
        logger.error(output)
        logger.error(f"{args.plugin} failure.")
