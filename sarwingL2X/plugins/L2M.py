from . import PluginArgs
from sarwingL2X import concat_ok
import os
from typing import Dict, Any
from subprocess import Popen, PIPE
import logging

logger = logging.getLogger(__name__)

qsubarray = os.getenv("QSUBARRAY", "")


def L2M(args: PluginArgs):
    logger.info("Starting plugin L2M")
    resolutions = args.plugin_config["resolutions"]

    use_qsubarray = args.plugin_config.get("use_qsubarray", args.use_qsubarray_default)
    ssh_host = args.plugin_config.get("ssh_host", args.ssh_host_default)

    if use_qsubarray:
        insert_qsub = f'export QSUBARRAY=\\"{qsubarray}\\" &&'
    else:
        insert_qsub = ""

    if ssh_host:
        condash_location = args.config["condash_source_location_remote"]
        ssh_host_cmd = f"rsh {ssh_host} -o SendEnv=QSUBARRAY \"bash -c '"
        ssh_host_cmd_end = "'\""
    else:
        condash_location = args.config["condash_source_location"]
        ssh_host_cmd = ""
        ssh_host_cmd_end = ""

    concatOkNc, concatOkComment = concat_ok(args.report_path)

    concatLines = ""
    for i, ncPath in enumerate(concatOkNc):
        concatLines += "%s %s\n" % (ncPath, concatOkComment[i])

    logger.info(f"Found {len(concatOkNc)} lines from concat_ok to be processed in L2M.")

    cmdStr = (
        f"{ssh_host_cmd} {args.post_ssh_cmd} "
        f"source {condash_location} && conda activate {args.custom_conda_env}/ && {insert_qsub} "
        f"swMultiResListing.py -d {args.out_dir} --reportPath {args.report_path} "
        f"{args.debug} {args.force_str} {' '.join([str(l_res) for l_res in resolutions])} {ssh_host_cmd_end}"
    )
    logger.info("Starting L2M generation... %s" % args.plugin)
    p = Popen(cmdStr, stdin=PIPE, shell=True, executable="/bin/bash")
    p.communicate(input=concatLines.encode("utf-8"))
    logger.info("Finished L2M generation.")
