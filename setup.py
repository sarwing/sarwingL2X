import os
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "sarwingL2X",
    use_scm_version=True,
    author = "Theo Cevaer",
    author_email = "Theo.Cevaer@ifremer.fr",
    description = ("Sarwing L2X processor"),
    license = "GPL",
    keywords = "science",
    url = "https://gitlab.ifremer.fr/sarwing/sarwingL2M.git",
    packages=find_packages(),
    include_package_data=True,
    package_data={
        # Include all HTML files in the 'templates' directory within your package
        'sarwingL2X': ['templates/*.html'],
    },
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 2 - Alpha",
        "Topic :: Scientific/Engineering",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
    ],
    install_requires=[
          'jinja2', "geopandas", 'numpy', 'netCDF4', 'matplotlib', 'shapely',
          "PyYAML", "psycopg2",
          'owi @ git+https://gitlab.ifremer.fr/sarwing/owi.git',
          'pathurl @ git+https://gitlab.ifremer.fr/sarwing/pathurl.git',
          'colormap_ext @ git+https://gitlab.ifremer.fr/sarwing/colormap_ext.git',
          'sarwingL2C @ git+https://gitlab.ifremer.fr/sarwing/sarwingL2C.git',
          'sarwingL2M @ git+https://gitlab.ifremer.fr/sarwing/sarwingL2M.git',
          'owiQL @ git+https://gitlab.ifremer.fr/sarwing/owiQL.git',
    ],
    scripts=[
        'sarwingL2X/swL2XReport.py',
        'sarwingL2X/swL2XProcessor.py',
        'sarwingL2X/pluginWrapper.sh'
    ]    
)
