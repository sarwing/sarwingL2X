import base64
import logging

logger = logging.getLogger(__name__)

import datetime
import shutil
import subprocess

from . import PluginArgs, write_content_to_unique_file, check_status
import os
from sarwingL2X import l2m_ok
import glob
import pandas as pd
from cyclobs_utils import cyclone_speed_dir_angle_to_north
import owi
from urllib.parse import quote


def api_data_for_center(
    api_url,
    df_all_api_atcf,
    df_all_api_ibtracs,
    sat_file_ll_gd_3km,
    commit,
    config_name,
    listing,
):
    df_all_api_atcf["filename"] = df_all_api_atcf["data_url"].apply(
        lambda x: os.path.basename(x)
    )

    df_all_api_ibtracs["filename"] = df_all_api_ibtracs["data_url"].apply(
        lambda x: os.path.basename(x)
    )

    ll_file = os.path.basename(sat_file_ll_gd_3km)
    df_atcf_for_file = df_all_api_atcf.loc[df_all_api_atcf["filename"] == ll_file]
    if len(df_atcf_for_file.index) == 0:
        return False
    d = df_atcf_for_file.iloc[0]

    df_ibtracs_for_file = df_all_api_ibtracs.loc[
        df_all_api_ibtracs["filename"] == ll_file
    ]
    if len(df_ibtracs_for_file.index) > 0:
        do = df_ibtracs_for_file.iloc[0]
    else:
        do = False

    sat_date = owi.getDateFromFname(sat_file_ll_gd_3km)
    direction, dir_std, speed, speed_std = cyclone_speed_dir_angle_to_north(
        df_atcf_for_file.iloc[0]["sid"],
        sat_date,
        base_url=api_url + "/app",
        commit=commit,
        config=config_name,
        listing=listing,
    )

    #     parser.add_argument("--track-point-wkt", required=True, type=str)
    #     parser.add_argument("--track-vmax", required=True, type=float)
    #     parser.add_argument("--track-rmw", required=True, type=float)
    #     parser.add_argument("--track-sid", required=True, type=str)
    #     parser.add_argument("--track-filepath", required=True, type=str)
    #     parser.add_argument("--track-rads34", required=True, type=str)
    #     parser.add_argument("--track-source-name", required=True, type=str)
    #
    #     parser.add_argument("--other-track-point-wkt", required=False, type=str)
    #     parser.add_argument("--other-track-vmax", required=False, type=float)
    #     parser.add_argument("--other-track-rmw", required=False, type=float)
    #     parser.add_argument("--other-track-source-name", required=False, type=str)
    #
    #     parser.add_argument("--cyclone-name", required=True, type=str)
    #     parser.add_argument("--sub-basin", required=True, type=str)
    #     parser.add_argument("--mission", required=True, type=str)
    #     parser.add_argument("--acq-start-date", required=True, type=str)
    #
    #     parser.add_argument("--cyclone-speed", required=True, type=str)
    #     parser.add_argument("--cyclone-speed-std", required=True, type=str)
    #     parser.add_argument("--cyclone-direction", required=True, type=str)
    #     parser.add_argument("--cyclone-direction-std", required=True, type=str)

    atcf_rads_names = ["rad34_seq", "rad34_neq", "rad34_swq", "rad34_nwq"]
    rads34 = []
    for r in atcf_rads_names:
        if d[r] is not None and d[r] != "":
            rads34.append(str(d[r]))
    rads34 = ",".join(rads34)
    data = {
        "cyclone-name": d["cyclone_name"],
        "sub-basin": d["sub_basin"],
        "mission": d["mission"],
        "acq-start-date": d["acquisition_start_time"],
        "track-point-wkt": d["track_point"],
        "track-vmax": d["vmax (m/s)"],
        "track-rmw": d["rmw"],
        "track-sid": d["sid"],
        "track-file": d["track_file"],
        "track-source-name": "atcf",
        "cyclone-speed": speed,
        "cyclone-speed-std": speed_std,
        "cyclone-direction": direction,
        "cyclone-direction-std": dir_std,
    }

    if do is not False:
        data.update(
            {
                "other-track-point-wkt": do["track_point"],
                "other-track-vmax": do["vmax (m/s)"],
                "other-track-rmw": do["rmw"],
                "other-track-source-name": "ibtracs",
            }
        )

    if rads34 != "":
        data["track-rads34"] = rads34

    return data


def all_api_data_for_center(api_url, commit, config, listing):
    api_atcf_req = f"{api_url}/app/api/getData?include_cols=all&instrument=C-Band_SAR&commit={quote(commit)}&config={quote(config)}&listing={quote(listing)}"
    logger.info(f"Getting data from Cyclobs API. Req : {str(api_atcf_req)}")
    df_atcf = pd.read_csv(api_atcf_req, keep_default_na=False)

    api_r = f"{api_url}/app/api/getData?include_cols=all&instrument=C-Band_SAR&track_source=ibtracs&commit={quote(commit)}&config={quote(config)}&listing={quote(listing)}"
    logger.info(f"Getting IBTRACS data from Cyclobs API. Req : {str(api_r)}")
    df_ibtracs_data = pd.read_csv(api_r, keep_default_na=False)

    return df_atcf, df_ibtracs_data


def tcva(args: PluginArgs):
    logger.info(f"Found plugin {args.plugin}")

    post_proc_dir = args.config["post_processing_dir"]
    api_host = args.plugin_config["api_host"]

    use_qsubarray = args.plugin_config.get("use_qsubarray", args.use_qsubarray_default)
    ssh_host = args.plugin_config.get("ssh_host", args.ssh_host_default)

    if use_qsubarray:
        qsubarray = args.plugin_config.get("qsubarray", os.getenv("QSUBARRAY", ""))
    else:
        qsubarray = ""

    if ssh_host:
        condash_location = args.config["condash_source_location_remote"]
        ssh_host_cmd = f"rsh {ssh_host} 'bash -c \""
        ssh_host_cmd_end = "\"'"
    else:
        condash_location = args.config["condash_source_location"]
        ssh_host_cmd = ""
        ssh_host_cmd_end = ""

    l2m_lines = l2m_ok(args.report_path)
    l2m_path = set([os.path.dirname(line) for line in l2m_lines])

    # For this plugin we want to use nclight_L2M 3km ll_gd. So we find these files
    nclight_ll_gd_3km_dirs = [
        os.path.join(path, post_proc_dir, "nclight_L2M_ll_gd") for path in l2m_path
    ]
    files_candidates = []
    for nclight_path in nclight_ll_gd_3km_dirs:
        nc_file = glob.glob(os.path.join(nclight_path, "*-*-*-*-*-*-*0003-*_ll_gd.nc"))
        if len(nc_file) > 0:
            nc_file = nc_file[0]
            output_dir = os.path.join(os.path.dirname(nc_file), "..", args.plugin)
            status_glob = "*"
            if not check_status(
                output_dir,
                status_glob,
                valid_status=["0", "20", "10", "12", "20", "30", "105"],
            ):
                files_candidates.append(nc_file)

    logger.info(f"Found {len(files_candidates)} ll_gd files.")

    df_all_atcf, df_all_ibtracs = all_api_data_for_center(
        api_host, commit=args.commit, config=args.config_name, listing=args.listing_name
    )

    absolute_path_wrapper = shutil.which("pluginWrapper.sh")
    script = "gen_analysis_fix_no_api.py"

    input_args = []
    for nc_file in files_candidates:
        args_data = api_data_for_center(
            api_host,
            df_all_atcf,
            df_all_ibtracs,
            nc_file,
            commit=args.commit,
            config_name=args.config_name,
            listing=args.listing_name,
        )
        if args_data is False:
            logger.warning(f"No API data for {os.path.basename(nc_file)}. Skipping.")
            continue

        args_data["sat-file-ll-gd"] = nc_file
        args_data["sat-file-sw"] = nc_file.replace(
            "nclight_L2M_ll_gd", "nclight_L2M_sw"
        ).replace("_ll_gd.nc", "_sw.nc")

        outpath = os.path.join(os.path.dirname(nc_file), "..", args.plugin)
        args_data["path"] = outpath
        args_data["resolution"] = "3"

        script_args = f"{nc_file} "
        for arg_name, arg_val in args_data.items():
            script_args += f'--{arg_name} "{arg_val}" '

        script_args_encoded = base64.b64encode(script_args.encode()).decode()
        input_args.append(script_args_encoded)

    bash_script = f"""#!/bin/bash
ENCODED_ARGS=$1
IN_ARGS=$(echo "$ENCODED_ARGS" | base64 --decode)
"""
    bash_script += 'IN_FILE="${IN_ARGS%% *}"\n'
    bash_script += 'IN_ARGS=${IN_ARGS#"$IN_FILE"}\n'
    bash_script += 'echo "yey$IN_ARGS"\n'
    bash_script += f'eval "{absolute_path_wrapper} {script} {args.plugin} \\"$IN_FILE\\" ../{args.plugin} {args.plugin} $IN_ARGS"\n'
    bsh_script_file = write_content_to_unique_file(
        args.config["tmp_script_dir"], bash_script
    )

    # pluginWrapper.sh "command_plugin_exec" "plugin_name" "path_in" "path_out" "identifier" ...
    # cmdStr = (f"source {args.custom_conda_env}/bin/activate && "
    cmdStr = (
        f"{ssh_host_cmd} {args.post_ssh_cmd} source {condash_location} && conda activate {args.custom_conda_env}/ && "
        f'{qsubarray} xargs -I ITEM {bsh_script_file} "ITEM" {ssh_host_cmd_end}'
    )
    logger.info("Starting %s plugin execution..." % args.plugin)
    logger.info(f"Command: {cmdStr}")
    p = subprocess.Popen(
        cmdStr, stdin=subprocess.PIPE, shell=True, executable="/bin/bash"
    )
    p.communicate(input="\n".join(input_args).encode("utf-8"))
    logger.info("Finished %s execution." % args.plugin)
