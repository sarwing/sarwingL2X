import logging
import traceback

logger = logging.getLogger(__name__)

from . import PluginArgs
import os
import pandas as pd
from urllib.parse import quote


def acq_csv(args: PluginArgs):
    logger.info(f"Found plugin {args.plugin}")

    cyclobs_url = args.plugin_config["cyclobs_web_url"]
    commit = args.commit
    config = args.config_name
    listing = args.listing_name

    plugin_dir = os.path.join(args.standalone_plugin_dir, args.plugin)
    os.makedirs(plugin_dir, exist_ok=True)

    try:
        # Make a request to cyclobs_url/app/api/getData?include_cols=all&commit=commit&config=config&listing=listing
        df = pd.read_csv(
            f"{cyclobs_url}/app/api/getData?include_cols=all&commit={quote(commit)}&config={quote(config)}&listing={quote(listing)}"
        )
        df.to_csv(f"{plugin_dir}/{args.plugin}.csv", index=False)
        output = f"CSV file written to {plugin_dir}/{args.plugin}.csv"
        logger.info(f"{args.plugin} executed successfully")
        returncode = "0"
    except Exception as e:
        # Other errors (like no command found)
        output = traceback.format_exc()
        logger.error(f"Error when executing {args.plugin} : {output}")
        returncode = "1"

    log_file = os.path.join(plugin_dir, f"{args.plugin}.log")
    status_file = os.path.join(plugin_dir, f"{args.plugin}.status")
    # Write the output to the log file
    with open(log_file, "w") as file:
        file.write(output)
    with open(status_file, "w") as file:
        file.write(returncode)
