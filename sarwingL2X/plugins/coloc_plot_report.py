import logging

logger = logging.getLogger(__name__)

from . import PluginArgs
import pandas as pd
import os
from jinja2 import Environment, FileSystemLoader
from pkg_resources import resource_filename

# pathurl doesn't work for unknown reason
def to_url(path):
    return path.replace("/home/datawork-cersat-public/cache/public/ftp/project/sarwing", "https://cyclobs.ifremer.fr/static/sarwing_datarmor")


def coloc_plot_report(args: PluginArgs):
    logger.info(f"Found plugin {args.plugin}")
    save_dir = os.path.join(args.standalone_plugin_dir, args.plugin)
    os.makedirs(save_dir, exist_ok=True)

    # Log file
    log_file = os.path.join(save_dir, f"{args.plugin}.log")
    log_handler = logging.FileHandler(log_file)
    log_handler.setLevel(logging.INFO)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    log_handler.setFormatter(formatter)
    logger.addHandler(log_handler)

    # status file
    status_file = os.path.join(save_dir, f"{args.plugin}.status")

    try:
        coloc_sources = args.plugin_config["coloc_sources"]

        for coloc_source in coloc_sources:
            coloc_source = coloc_source.lower()
            listing_path = os.path.join(
                args.report_path, f"coloc_{coloc_source}_processok.csv"
            )
            if not os.path.exists(listing_path):
                logger.warning(
                    f"Couldn't find listing for coloc {coloc_source} : {listing_path}"
                )
                continue

            plot_report_dir = os.path.join(args.standalone_plugin_dir, args.plugin)
            os.makedirs(plot_report_dir, exist_ok=True)

            # Step 1: Read the CSV and get the 'File' column
            df = pd.read_csv(listing_path)

            # Path to the Jinja2 template
            template_dir = resource_filename("sarwingL2X", "templates")
            env = Environment(loader=FileSystemLoader(template_dir))
            template = env.get_template("coloc_plot_report.html")

            # Prepare data for the HTML report
            report_data = []
            for index, row in df.iterrows():
                plot_dir = os.path.join(
                    os.path.dirname(row["File"]), f"../coloc_{coloc_source}_plots"
                )  # Modify as needed
                if not os.path.exists(plot_dir):
                    logger.warning(f"Couldn't find plot dir : {plot_dir}")
                    report_data.append(
                        {
                            "file_path": row["File"],
                            "plot_paths": [],
                            "index_html": "",
                            "comments": "",
                        }
                    )
                    continue
                plot_files = sorted(
                    [f for f in os.listdir(plot_dir) if f.endswith(".png")],
                    key=lambda x: x.lower(),
                )  # Assuming PNG plots
                plot_paths = [
                    to_url(os.path.join(plot_dir, f))
                    for f in plot_files
                ]
                index_html_path = os.path.join(plot_dir, "index.html")
                report_data.append(
                    {
                        "file_path": os.path.basename(row["File"]),
                        "full_file_path": to_url(row["File"]),
                        "plot_paths": plot_paths,
                        "index_html": to_url(index_html_path),
                        "comments": row["Comments"],
                    }
                )

            main_report_path_url = to_url(
                os.path.join(args.report_path, "index.html")
            )
            context_info = [
                {"label": "Number of files", "value": len(df)},
                {
                    "label": f"Coloc {coloc_source.upper()} CSV listing file",
                    "value": f'<a href="{to_url(listing_path)}">{os.path.basename(listing_path)}</a>',
                },
                {
                    "label": f"Coloc {coloc_source.upper()} TXT listing file",
                    "value": f'<a href="{to_url(listing_path.replace(".csv", ".txt"))}">{os.path.basename(listing_path.replace(".csv", ".txt"))}</a>',
                },
                # Add more context info as needed
                {"label": "Listing Name", "value": args.listing_name},
                {"label": "Commit", "value": args.commit},
                {"label": "Config", "value": args.config_name},
                {"label": "Coloc type", "value": f"SAR-{coloc_source.upper()}"},
                {
                    "label": "Main Report",
                    "value": f'<a href="{main_report_path_url}">Go back to main report</a>',
                },
            ]

            # Step 4: Generate the HTML report
            html_content = template.render(
                files=report_data,
                context=context_info,
                coloc_source=coloc_source.upper(),
            )

            outfile = os.path.join(
                args.standalone_plugin_dir,
                args.plugin,
                f"coloc_{coloc_source}_plot_report.html",
            )
            # Write the HTML content to a file
            with open(outfile, "w", encoding="utf-8") as f:
                f.write(html_content)

            logger.info(f"Coloc plot report generated successfully to {outfile}.")
            with open(status_file, "w") as f:
                f.write("0")
    except Exception as e:
        logger.error(e)
        with open(log_file, "a") as f:
            f.write(str(e))
        with open(status_file, "w") as f:
            f.write("1")
