import logging

logger = logging.getLogger(__name__)

from . import PluginArgs, check_status
from ..constants import *
from sarwingL2X import checkStatus, checkVersion
import os
from typing import Dict, Any
import owiQL
import csv
from subprocess import Popen, PIPE


def QL(args: PluginArgs):
    post_proc_dir = args.config["post_processing_dir"]

    use_qsubarray = args.plugin_config.get("use_qsubarray", args.use_qsubarray_default)
    ssh_host = args.plugin_config.get("ssh_host", args.ssh_host_default)

    if use_qsubarray:
        qsubarray = args.plugin_config.get("qsubarray", os.getenv("QSUBARRAY", ""))
    else:
        qsubarray = ""

    if ssh_host:
        condash_location = args.config["condash_source_location_remote"]
        ssh_host_cmd = f"rsh {ssh_host} 'bash -c \""
        ssh_host_cmd_end = "\"'"
    else:
        condash_location = args.config["condash_source_location"]
        ssh_host_cmd = ""
        ssh_host_cmd_end = ""

    QL_DIR = "QL"
    QL_NAME = "QL"

    logger.info("Starting plugin QL")
    QLforce = args.force

    dirsToProcess = []
    with open(os.path.join(args.report_path, CONCAT_CSV), "r") as csvReport:
        reportDict = csv.DictReader(csvReport)
        for row in reportDict:
            dirPath = os.path.dirname(row["L2C file"])
            statusFile = os.path.join(dirPath, CONCAT_STATUS)
            if not checkStatus(statusFile, status_ok=["0", "3"]):
                # status 3 : gap too large, but we want a QL
                QLstatusPath = os.path.join(
                    dirPath, post_proc_dir, args.plugin, f"{args.plugin}.status"
                )
                QLversionPath = os.path.join(
                    dirPath, post_proc_dir, args.plugin, f"{args.plugin}.version"
                )
                if (
                    QLforce
                    or checkStatus(QLstatusPath)
                    or checkVersion(QLversionPath, owiQL.version)
                ):
                    dirsToProcess.append(dirPath)

    dirsStr = "\n".join(dirsToProcess)
    if dirsStr != "":
        dirsStr += "\n"
        cmdStr = (
            f"{ssh_host_cmd} {args.post_ssh_cmd} source {condash_location} && conda activate {args.custom_conda_env} && "
            f"{qsubarray} xargs -L 1 -r swQLWrapper.sh {args.debug} -i {ssh_host_cmd_end}"
        )
        logger.info(f"Found {len(dirsToProcess)} files to process.")
        logger.info("Starting QL generation...")
        logger.info(f"Command: {cmdStr}")
        p = Popen(cmdStr, stdin=PIPE, shell=True, executable="/bin/bash")
        p.communicate(input=dirsStr.encode("utf-8"))
    else:
        logger.info("Nothing to do for QL")
    logger.info("Finished QL generation.")
