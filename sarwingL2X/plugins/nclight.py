import logging

logger = logging.getLogger(__name__)

from . import PluginArgs, write_content_to_unique_file, check_status
from sarwingL2X import concat_ok, l2m_ok
import os
import glob
from subprocess import Popen, PIPE
import shutil


def nclight(args: PluginArgs):
    post_proc_dir = args.config["post_processing_dir"]
    mode = args.plugin_config["mode"]
    source = args.plugin_config["source"]
    p_id = args.plugin_config["id"]

    use_qsubarray = args.plugin_config.get("use_qsubarray", args.use_qsubarray_default)
    ssh_host = args.plugin_config.get("ssh_host", args.ssh_host_default)

    if use_qsubarray:
        qsubarray = args.plugin_config.get("qsubarray", os.getenv("QSUBARRAY", ""))
    else:
        qsubarray = ""

    if ssh_host:
        condash_location = args.config["condash_source_location_remote"]
        ssh_host_cmd = f"rsh {ssh_host} 'bash -c \""
        ssh_host_cmd_end = "\"'"
    else:
        condash_location = args.config["condash_source_location"]
        ssh_host_cmd = ""
        ssh_host_cmd_end = ""

    logger.info(f"Starting plugin {p_id}")
    file_suffix = mode

    additionnal_args = ""
    if mode == "_sw":
        script = "owi2nclight.py"
        path_in_wrapper = "$(dirname $1)"
        path_out_wrapper = f"{post_proc_dir}/{p_id}"
        path_in_script = "$1"
        path_out_script = (
            f"$(dirname $1)/{post_proc_dir}/{p_id}/$(basename $1 .nc){file_suffix}.nc"
        )
        identifier = f"{p_id}_$(basename $1 .nc){file_suffix}"
    elif (
        mode == "_gd" or mode == "_ll_gd"
    ):  # Remember, in these modes we must use _sw file as input
        script = "nclight2gridded.py"
        if mode == "_ll_gd":
            additionnal_args = "--crs=epsg:4326"
        path_in_wrapper = "$(dirname $1)"
        path_out_wrapper = f"../{p_id}"
        path_in_script = "$1"
        path_out_script = (
            f"$(dirname $1)/../{p_id}/$(basename $1 _sw.nc){file_suffix}.nc"
        )
        identifier = f"{p_id}_$(basename $1 _sw.nc){file_suffix}"
    else:
        err = f"Unknown nclight mode: {mode}"
        logger.error(err)
        raise ValueError(err)

    if source == "L2C":
        if mode == "_sw":
            # Input files are concatenated files
            concatOkNc, concatOkComment = concat_ok(args.report_path)
            source_dirs = [os.path.dirname(nc) for nc in concatOkNc]
        elif mode == "_ll_gd" or mode == "_gd":
            concatOkNc, concatOkComment = concat_ok(args.report_path)
            source_dirs = [os.path.dirname(nc) for nc in concatOkNc]
            # Define _sw nclight path post_proc as input
            source_dirs = [
                os.path.join(
                    nc_dir,
                    post_proc_dir,
                    p_id.replace("_ll_gd", "_sw").replace("_gd", "_sw"),
                )
                for nc_dir in source_dirs
            ]

    elif source == "L2M":
        L2M = "L2M"

        # Input files are concatenated files
        # concatOkNc, concatOkComment = concat_ok(args.report_path)

        l2m_lines = l2m_ok(args.report_path)
        l2m_path = set([os.path.dirname(line) for line in l2m_lines])

        if mode == "_sw":
            source_dirs = l2m_path
        elif (
            mode == "_ll_gd" or mode == "_gd"
        ):  # Remember, in these modes we must use _sw file as input
            # Define _sw nclight path post_proc as input
            source_dirs = [
                os.path.join(
                    p,
                    post_proc_dir,
                    p_id.replace("_ll_gd", "_sw").replace("_gd", "_sw"),
                )
                for p in l2m_path
            ]
    else:
        err = f"Unknown nclight source: {source}"
        logger.error(err)
        raise ValueError(err)

    # Finding all nc files in selected dirs
    source_files = []
    for dirs in source_dirs:
        if mode == "_sw":
            files = glob.glob(os.path.join(dirs, "*.nc"))
        elif mode == "_ll_gd" or mode == "_gd":
            files = glob.glob(os.path.join(dirs, "*_sw.nc"))
        if len(files) > 0:
            source_files.extend(files)

    to_process = []
    for source_file in source_files:
        output_dir = os.path.join(os.path.dirname(source_file), path_out_wrapper)
        if mode == "_sw":
            status_file_glob = f"{p_id}_{os.path.basename(source_file).replace('.nc', '')}{file_suffix}"
        else:
            status_file_glob = f"{p_id}_{os.path.basename(source_file).replace('_sw.nc', '')}{file_suffix}"
        if not check_status(output_dir, status_file_glob):
            to_process.append(source_file)

    to_process_lines = "\n".join(to_process)

    absolute_path_wrapper = shutil.which("pluginWrapper.sh")

    bash_script = "#!/bin/bash\n"
    bash_script += (
        f"{absolute_path_wrapper} {script} {p_id} "
        f"{path_in_wrapper} "
        f"{path_out_wrapper} {identifier} "
        f"{additionnal_args} {path_in_script} "
        f"{path_out_script}"
    )

    logger.info(bash_script)

    bsh_script_file = write_content_to_unique_file(
        args.config["tmp_script_dir"], bash_script
    )

    # pluginWrapper.sh "command_plugin_exec" "plugin_name" "path_in" "path_out_relative" "identifier" ...
    cmdStr = (
        f"{ssh_host_cmd} {args.post_ssh_cmd} source {condash_location} && conda activate {args.custom_conda_env}/ && "
        f'{qsubarray} xargs -L 1 -I ITEM {bsh_script_file} "ITEM" {ssh_host_cmd_end}'
    )
    logger.info("Starting %s plugin execution..." % p_id)
    logger.info(f"Command: {cmdStr}")
    logger.info(f"Found {len(to_process)} to process.")
    p = Popen(cmdStr, stdin=PIPE, shell=True, executable="/bin/bash")
    p.communicate(input=to_process_lines.encode("utf-8"))
    logger.info("Finished %s execution." % args.plugin)
