import glob
from dataclasses import dataclass
from typing import Dict, Any
import os
import uuid


@dataclass
class PluginArgs:
    plugin: str
    out_dir: str
    force: bool
    force_str: str
    report_path: str
    standalone_plugin_dir: str
    listing_name: str
    config: Dict[str, Any]
    plugin_config: Dict[str, Any]
    custom_conda_env: str
    commit: str
    config_name: str  # this is the config name used for the listing
    debug: str
    use_qsubarray_default: bool
    ssh_host_default: bool | str
    post_ssh_cmd: bool | str


def write_content_to_unique_file(directory, content, ext="sh"):
    """
    Writes content to a uniquely named file within the specified directory.

    :param directory: The directory path where the file will be created.
    :param content: The content to write to the file.
    :return: The name of the file after the content has been written.
    """
    # Ensure the directory exists
    if not os.path.exists(directory):
        os.makedirs(directory)

    # Generate a unique filename
    unique_filename = f"{uuid.uuid4()}.{ext}"
    file_path = os.path.join(directory, unique_filename)

    # Write the content to the file
    with open(file_path, "w") as file:
        file.write(content)

    # Make file executable
    os.chmod(file_path, 0o755)

    # Return the unique filename for further reference
    return file_path


def check_status(status_dir, status_file_glob="*", valid_status=["0"]):
    status_files = glob.glob(os.path.join(status_dir, f"{status_file_glob}.status"))

    if len(status_files) == 0:
        return False

    for f in status_files:
        with open(f) as open_f:
            status = open_f.readline().strip()
            if status not in valid_status:
                return False
    return True
