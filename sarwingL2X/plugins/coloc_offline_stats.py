import logging
import shutil

logger = logging.getLogger(__name__)

from . import PluginArgs
import os
import subprocess
import traceback
from subprocess import Popen, PIPE


def coloc_offline_stats(args: PluginArgs):
    logger.info(f"Found plugin {args.plugin}")

    plugins_coloc = args.plugin_config["plugins_coloc"]
    force = args.plugin_config.get("force", False)
    use_qsubarray = args.plugin_config.get("use_qsubarray", args.use_qsubarray_default)
    use_ssh = args.plugin_config.get("use_ssh", False)
    ssh_host = args.plugin_config.get("ssh_host", args.ssh_host_default)
    condash_location = args.config["condash_source_location"]

    if use_qsubarray:
        qsubarray = args.plugin_config.get("qsubarray", os.getenv("QSUBARRAY", ""))
    else:
        qsubarray = ""

    if use_ssh:
        condash_location = args.config["condash_source_location_remote"]
        ssh_host_cmd = f"rsh {ssh_host} 'bash -c \""
        ssh_host_cmd_end = "\"'"
    else:
        condash_location = args.config["condash_source_location"]
        ssh_host_cmd = ""
        ssh_host_cmd_end = ""

    plugin_dir = os.path.join(args.standalone_plugin_dir, args.plugin)
    os.makedirs(plugin_dir, exist_ok=True)

    # read status, do not execute if status == 0
    status_file = os.path.join(plugin_dir, f"{args.plugin}.status")
    if not force and os.path.exists(status_file):
        with open(status_file, "r") as file:
            status = file.read().strip()
        if status == "0":
            logger.info(f"{args.plugin} already executed")
            return

    cmd_str = (
        f"{ssh_host_cmd} {args.post_ssh_cmd} source {condash_location} && conda activate {args.custom_conda_env}/ && "
        f'{qsubarray}  xargs -ICMD bash -c "CMD" {ssh_host_cmd_end}'
    )

    logger.info("Reading coloc listings...")

    all_coloc_listings = []
    for plugin in plugins_coloc:
        listing_file = os.path.join(args.report_path, f"{plugin}_processok.txt")
        if not os.path.exists(listing_file):
            logger.warning(f"Listing file {listing_file} does not exist. Skipping.")
            continue

        all_coloc_listings.append(listing_file)

    if len(all_coloc_listings) == 0:
        logger.warning("No coloc listings found. Exiting {args.plugin}.")
        return

    absolute_path_wrapper = shutil.which("pluginWrapper.sh")

    # Using pluginWrapper to have logs and status
    # pluginWrapper.sh "command_plugin_exec" "plugin_name" "path_in" "path_out" "identifier" "arg1_to_plugin" "arg2_to_plugin" ...
    script_cmd = (f"{absolute_path_wrapper} gen_coloc_plots.py {args.plugin} {args.standalone_plugin_dir} {args.plugin} {args.plugin} -e {plugin_dir} "
                  f"--commit {args.commit} --config {args.config_name} --listing-name {args.listing_name}")

    # script_cmd = f"gen_coloc_plots.py -e {plugin_dir}"

    for listing_file in all_coloc_listings:
        script_cmd += f" -l {listing_file}"

    logger.info(f"Starting {args.plugin} plugin execution...")
    logger.info(f"Command: {cmd_str}")

    # Calling cyclobs_offline command
    try:
        # Execute the command
        process = subprocess.Popen(
            cmd_str,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            executable="/bin/bash",
            text=True,
            shell=True,
        )
        output, _ = process.communicate(input=script_cmd)
        returncode = str(process.returncode)
    except subprocess.CalledProcessError as e:
        # This gives you the output of the failed command
        output = e.output
        returncode = str(e.returncode)
    except Exception:
        # Other errors (like no command found)
        output = traceback.format_exc()
        returncode = "1"

    log_file = os.path.join(plugin_dir, f"{args.plugin}_job.log")
    status_file = os.path.join(plugin_dir, f"{args.plugin}_job.status")
    # Write the output to the log file
    with open(log_file, "w") as file:
        file.write(output)
    with open(status_file, "w") as file:
        file.write(returncode)
    print(output)
    # Check if the command was successful
    if returncode == "0":
        logger.info(output)
        logger.info(f"{args.plugin} plugin success")
    else:
        logger.error(output)
        logger.error(f"{args.plugin} failure.")
