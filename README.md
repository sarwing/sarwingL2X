# sarwingL2X

eXtend sarwing L2 processing:
  * concatenation (L2C)
  * multiresolution (L2M)

## Installation

  ```
  conda install matplotlib=2.0.2 netcdf4 descartes basemap=1.0.7 pandas shapely scipy numpy faulthandler
  pip install git+https://gitlab.ifremer.fr/sarwing/sarwingL2M.git
  ```