import logging

logger = logging.getLogger(__name__)


import shutil

from . import PluginArgs, write_content_to_unique_file, check_status
import os
import yaml
from sarwingL2X import l2m_ok
from subprocess import Popen, PIPE
import glob


def coloc_smos(args: PluginArgs):
    logger.info(f"Found plugin {args.plugin}")

    post_proc_dir = args.config["post_processing_dir"]
    commit_index = int(args.config["commit_index"])
    config_index = int(args.config["config_index"])
    configs_directory = args.config["configs_directory"]
    delta_time = args.plugin_config["maximum_delta_time_min"]
    minimal_area = args.plugin_config["minimal_area_km"]

    use_qsubarray = args.plugin_config.get("use_qsubarray", args.use_qsubarray_default)
    ssh_host = args.plugin_config.get("ssh_host", args.ssh_host_default)

    if use_qsubarray:
        qsubarray = args.plugin_config.get("qsubarray", os.getenv("QSUBARRAY", ""))
    else:
        qsubarray = ""

    if ssh_host:
        condash_location = args.config["condash_source_location_remote"]
        ssh_host_cmd = f"rsh {ssh_host} 'bash -c \""
        ssh_host_cmd_end = "\"'"
    else:
        condash_location = args.config["condash_source_location"]
        ssh_host_cmd = ""
        ssh_host_cmd_end = ""

    # Build config.yaml to feed to coloc_sat script
    # What we want :
    # paths:
    #   SMOS:
    #     - '/home/ref-smoswind-public/data/v3.0/l3/data/reprocessing/%Y/%(dayOfYear)/*%Y%m%d*.nc'
    #     - '/home/ref-smoswind-public/data/v3.0/l3/data/nrt/%Y/%(dayOfYear)/*%Y%m%d*.nc'
    #   RS2:
    #     L2:
    #       - '/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/<COMMIT_TO_SET><CONFIG>/RS2/*/*/%Y/%(dayOfYear)/RS2_OK*/RS2_*%Y%m%d*/post_processing/nclight_L2M/rs2*owi*%Y%m%d*0003*_ll_gd.nc'
    #   S1:
    #     L2:
    #       - '/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/<COMMIT_TO_SET>/<CONFIG>/sentinel-1*/*/*/*/%Y/%(dayOfYear)/S1*%Y%m%d*/post_processing/nclight_L2M/s1*owi*%Y%m%d*0003*_ll_gd.nc'
    #   RCM:

    #     L2:
    #       - '/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/<COMMIT_TO_SET>/<CONFIG>/rcm*/*/*/*/%Y/%(dayOfYear)/S1*%Y%m%d*/post_processing/nclight_L2M/rcm*owi*%Y%m%d*0003*_ll_gd.nc'

    # Need to retrieve COMMIT_TO_SET and CONFIG

    l2m_lines = l2m_ok(args.report_path)
    l2m_path = set([os.path.dirname(line) for line in l2m_lines])

    # For this plugin we want to use nclight_L2M 50km ll_gd. So we find these files
    nclight_ll_gd_3km_dirs = [
        os.path.join(path, post_proc_dir, "nclight_L2M_ll_gd") for path in l2m_path
    ]

    files_to_coloc = []
    for nclight_path in nclight_ll_gd_3km_dirs:
        nc_file = glob.glob(os.path.join(nclight_path, "*-*-*-*-*-*-*0050-*_ll_gd.nc"))
        if len(nc_file) > 0:
            nc_file = nc_file[0]
            output_dir = os.path.join(os.path.dirname(nc_file), '..', args.plugin)
            status_glob = "*"
            if not check_status(output_dir, status_glob, valid_status=["0", "20"]): # Status "20" means no coloc has been found
                files_to_coloc.append(nc_file)

    files_coloc_str = "\n".join(files_to_coloc)

    commit = args.report_path.split("/")[commit_index]
    config = args.report_path.split("/")[config_index]

    paths = {
        "SMOS": [
            "/home/ref-smoswind-public/data/v3.0/l3/data/reprocessing/%Y/%(dayOfYear)/*%Y%m%d*.nc",
            "/home/ref-smoswind-public/data/v3.0/l3/data/nrt/%Y/%(dayOfYear)/*%Y%m%d*.nc",
        ]
    }

    common_vars = {
        "wind_speed": "wind_speed",
        "wind_direction": "wind_direction_ecmwf",
        "wind_from_direction": "wind_from_direction",
        "longitude": "lon",
        "latitude": "lat",
        "time": "time",
    }

    os.makedirs(os.path.join(configs_directory, "tmp"), exist_ok=True)
    config_path_tmp = os.path.join(
        configs_directory,
        "tmp",
        f"l2x_coloc_smos_{commit}_{config}_{args.listing_name}.yaml",
    )
    with open(config_path_tmp, "w") as f:
        yaml.dump({"paths": paths, "common_var_names": common_vars}, f)

    coloc_script = "Coloc_between_product_and_mission"

    # usage: Coloc_between_product_and_mission [-h] [--product1-id PRODUCT1_ID] [--destination-folder [DESTINATION_FOLDER]] [--delta-time [DELTA_TIME]] [--minimal-area [MINIMAL_AREA]] [--mission-name [{S1,RS2,RCM,HY2,ERA5,WS,SMOS,SMAP}]] [--input-ds [INPUT_DS]] [--level [{1,2}]] [--listing]
    #                                          [--no-listing] [--product-generation] [--no-product-generation] [--listing-filename [LISTING_FILENAME]] [--colocation-filename [COLOCATION_FILENAME]] [--config CONFIG]
    #
    # Generate co-locations between a specified product and a mission.
    #
    # options:
    #   -h, --help            show this help message and exit
    #   --product1-id PRODUCT1_ID
    #                         Path of the first product.
    #   --destination-folder [DESTINATION_FOLDER]
    #                         Folder path for the output.
    #   --delta-time [DELTA_TIME]
    #                         Maximum time in minutes between two product acquisitions.
    #   --minimal-area [MINIMAL_AREA]
    #                         Minimal intersection area in square kilometers.
    #   --mission-name [{S1,RS2,RCM,HY2,ERA5,WS,SMOS,SMAP}]
    #                         Name of the dataset to be compared.
    #   --input-ds [INPUT_DS]
    #                         Subset of mission products to compare with. It is a txt file that contains these paths.
    #   --level [{1,2}]       Product level (SAR missions only).
    #   --listing             Create a listing of co-located files.
    #   --no-listing          Do not create a listing.
    #   --product-generation  Generate a co-location product.
    #   --no-product-generation
    #                         Do not generate a co-location product.
    #   --listing-filename [LISTING_FILENAME]
    #                         Name of the listing file to be created.
    #   --colocation-filename [COLOCATION_FILENAME]
    #                         Name of the co-location product to be created.
    #   --config CONFIG       Configuration file to use instead of the default one.

    # We want that a "coloc_smos" dir is created in post_processing. For that we have to give this path as output.
    # But we want nclight 40km as input. Using the suffix to reach the correct dir.
    # pathin = L2C_path/.../post_processing/nclight_L2M/file.nc
    # pathout_suffix = ../{args.plugin}

    absolute_path_wrapper = shutil.which("pluginWrapper.sh")

    bash_script = f"""#!/bin/bash
IN_FILE=$1
FILEDIR=$(dirname $1)
{absolute_path_wrapper} {coloc_script} {args.plugin} $IN_FILE ../{args.plugin} {args.plugin} """
    bash_script += (
        f"--product1-id $IN_FILE --destination-folder $FILEDIR/../{args.plugin} "
        f"--delta-time {delta_time} "
        f"--minimal-area {minimal_area} "
        f"--mission-name SMOS "
        f"--no-listing "
        f"--product-generation "
        f"--config {config_path_tmp}"
    )

    bsh_script_file = write_content_to_unique_file(
        args.config["tmp_script_dir"], bash_script
    )

    # pluginWrapper.sh "command_plugin_exec" "plugin_name" "path_in" "path_out" "identifier" ...
    # cmdStr = (f"source {args.custom_conda_env}/bin/activate && "
    cmdStr = (
        f"{ssh_host_cmd} {args.post_ssh_cmd} source {condash_location} && conda activate {args.custom_conda_env}/ && "
        f'{qsubarray} xargs -L 1 -I ITEM {bsh_script_file} "ITEM" {ssh_host_cmd_end}'
    )
    logger.info("Starting %s plugin execution..." % args.plugin)
    p = Popen(cmdStr, stdin=PIPE, shell=True, executable="/bin/bash")
    p.communicate(input=files_coloc_str.encode("utf-8"))
    logger.info("Finished %s execution." % args.plugin)
