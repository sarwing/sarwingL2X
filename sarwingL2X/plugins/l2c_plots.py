import copy
import logging

logger = logging.getLogger(__name__)


import shutil

from . import PluginArgs, check_status
import os
import yaml
import pandas as pd
from sarwingL2X import concat_ok
from subprocess import Popen, PIPE
import glob
from . import write_content_to_unique_file


def l2c_plots(args: PluginArgs):
    logger.info(f"Found plugin {args.plugin}")

    post_proc_dir = args.config["post_processing_dir"]

    use_qsubarray = args.plugin_config.get("use_qsubarray", args.use_qsubarray_default)
    ssh_host = args.plugin_config.get("ssh_host", args.ssh_host_default)

    if use_qsubarray:
        qsubarray = args.plugin_config.get("qsubarray", os.getenv("QSUBARRAY", ""))
    else:
        qsubarray = ""

    if ssh_host:
        condash_location = args.config["condash_source_location_remote"]
        ssh_host_cmd = f"rsh {ssh_host} 'bash -c \""
        ssh_host_cmd_end = "\"'"
    else:
        condash_location = args.config["condash_source_location"]
        ssh_host_cmd = ""
        ssh_host_cmd_end = ""

    concat_lines_ok, concat_comments = concat_ok(args.report_path)

    # Process only status file in l2c_plots plugin that contains no status or bad status.
    nok_to_process = []
    for concat_file_ok in concat_lines_ok:
        l2c_plot_outdir = os.path.join(
            os.path.dirname(concat_file_ok), post_proc_dir, args.plugin
        )
        if not check_status(l2c_plot_outdir):
            nok_to_process.append(concat_file_ok)

    # For this plugin we want to use coloc files. So we find these files
    # coloc_dirs = [
    #    os.path.join(path, post_proc_dir, f"coloc_{coloc_source}") for path in l2m_path
    # ]
    #
    # files_to_plot = []
    # for coloc_path in coloc_dirs:
    #    nc_file = glob.glob(os.path.join(coloc_path, "*.nc"))
    #    if len(nc_file) > 0:
    #        files_to_plot.append(nc_file[0])

    logger.info(f"Found {len(nok_to_process)} files to plots.")
    infos = [
        f"<div><b>L2C filename</b>: {os.path.basename(nok_to_process[i])}</div><div><b>Cyclone infos</b>: {concat_comments[i]}</div>"
        for i in range(len(nok_to_process))
    ]

    combined_info = [f"{file}|{info}" for file, info in zip(nok_to_process, infos)]
    files_infos_str = "\n".join(combined_info)

    plot_script = "l2c_plot_report"
    absolute_path_wrapper = shutil.which("pluginWrapper.sh")

    # pluginWrapper.sh "command_plugin_exec" "plugin_name" "path_in" "path_out" "identifier" ...
    # plugin_name is for naming the folder containing files
    # identifier is for naming log and status files
    # cmdStr = (f"source {args.custom_conda_env}/bin/activate && "
    bash_script = f"""#!/bin/bash
FILEPATH=$(echo $1 | cut -d"|" -f1)
INFOS=$(echo $1 | cut -d"|" -f2)
DIRPATH=$(dirname "$FILEPATH")
{absolute_path_wrapper} {plot_script} {args.plugin} "$FILEPATH" {post_proc_dir}/{args.plugin} """
    bash_script += f"""{args.plugin} -i "$FILEPATH" -o "$DIRPATH/{post_proc_dir}/{args.plugin}" --infos "$INFOS"
    """

    bsh_script_file = write_content_to_unique_file(
        args.config["tmp_script_dir"], bash_script
    )

    cmdStr = (
        f"{ssh_host_cmd} {args.post_ssh_cmd} source {condash_location} && conda activate {args.custom_conda_env}/ && "
        f'{qsubarray} xargs -L 1 -I ITEM {bsh_script_file} "ITEM" {ssh_host_cmd_end}'
    )
    # cmdStr = (
    #    f"source {condash_location} && conda activate {args.custom_conda_env}/ && "
    #    f"{qsubarray} xargs -I FILEPATH sh -c '{absolute_path_wrapper} {plot_script} {p_id} $0 "
    #    f"../{p_id} "
    #    f"{p_id} "
    #    f"-i $0 "
    #    f"-o $(dirname $0)/../{p_id}' FILEPATH"
    # )
    logger.info("Starting %s plugin execution..." % args.plugin)
    p = Popen(cmdStr, stdin=PIPE, shell=True, executable="/bin/bash")
    p.communicate(input=files_infos_str.encode("utf-8"))
    logger.info("Finished %s execution." % args.plugin)
