import copy
import logging

logger = logging.getLogger(__name__)


import shutil

from . import PluginArgs, check_status
import os
import yaml
import pandas as pd
from sarwingL2X import l2m_ok
from subprocess import Popen, PIPE
import glob
from . import write_content_to_unique_file


def coloc_plots(args: PluginArgs):
    logger.info(f"Found plugin {args.plugin}")

    coloc_source = args.plugin_config["coloc_source"].lower()
    post_proc_dir = args.config["post_processing_dir"]

    use_qsubarray = args.plugin_config.get("use_qsubarray", args.use_qsubarray_default)
    ssh_host = args.plugin_config.get("ssh_host", args.ssh_host_default)

    if use_qsubarray:
        qsubarray = args.plugin_config.get("qsubarray", os.getenv("QSUBARRAY", ""))
    else:
        qsubarray = ""

    if ssh_host:
        condash_location = args.config["condash_source_location_remote"]
        ssh_host_cmd = f"rsh {ssh_host} 'bash -c \""
        ssh_host_cmd_end = "\"'"
    else:
        condash_location = args.config["condash_source_location"]
        ssh_host_cmd = ""
        ssh_host_cmd_end = ""

    p_id = f"coloc_{coloc_source}_plots"

    l2m_lines = l2m_ok(args.report_path)
    l2m_path = set([os.path.dirname(line) for line in l2m_lines])

    # For this plugin we want to use coloc files. So we find these files
    # coloc_dirs = [
    #    os.path.join(path, post_proc_dir, f"coloc_{coloc_source}") for path in l2m_path
    # ]
    #
    # files_to_plot = []
    # for coloc_path in coloc_dirs:
    #    nc_file = glob.glob(os.path.join(coloc_path, "*.nc"))
    #    if len(nc_file) > 0:
    #        files_to_plot.append(nc_file[0])
    coloc_processok_path = os.path.join(
        args.report_path, f"coloc_{coloc_source.lower()}_processok.csv"
    )
    df_coloc = pd.read_csv(coloc_processok_path)

    # Process only status file in coloc_plots plugin that contains no status or bad status.
    keep_to_process = []
    for index, row in df_coloc.iterrows():
        coloc_plot_outdir = os.path.join(os.path.dirname(row["File"]), "..", p_id)
        if not check_status(coloc_plot_outdir):
            keep_to_process.append(row["File"])

    df_coloc = df_coloc.loc[df_coloc["File"].isin(keep_to_process)]

    logger.info(f"Found {len(df_coloc.index)} files to plots.")

    df_coloc["coloc_file"] = df_coloc["File"].apply(lambda x: os.path.basename(x))
    df_coloc["info"] = df_coloc.apply(
        lambda x: f"<div><b>Coloc filename</b>: {x['coloc_file']}</div><div><b>Cyclone infos</b>: {x['Comments']}</div>",
        axis="columns",
    )
    combined_info = [
        f"{file}|{info}"
        for file, info in zip(df_coloc["File"].tolist(), df_coloc["info"].tolist())
    ]
    files_infos_str = "\n".join(combined_info)

    plot_script = "plot_coloc_file_report"
    absolute_path_wrapper = shutil.which("pluginWrapper.sh")

    bash_script = f"""#!/bin/bash
FILEPATH=$(echo $1 | cut -d"|" -f1)
INFOS=$(echo $1 | cut -d"|" -f2)
DIRPATH=$(dirname "$FILEPATH")
{absolute_path_wrapper} {plot_script} {p_id} "$FILEPATH" ../{p_id} """
    bash_script += f"""{p_id} -i "$FILEPATH" -o "$DIRPATH/../{p_id}" --infos "$INFOS"
        """

    bsh_script_file = write_content_to_unique_file(
        args.config["tmp_script_dir"], bash_script
    )

    # pluginWrapper.sh "command_plugin_exec" "plugin_name" "path_in" "path_out" "identifier" ...
    # plugin_name is for naming the folder containing files
    # identifier is for naming log and status files
    # cmdStr = (f"source {args.custom_conda_env}/bin/activate && "
    cmdStr = (
        f"{ssh_host_cmd} {args.post_ssh_cmd} source {condash_location} && conda activate {args.custom_conda_env}/ && "
        f'{qsubarray} xargs -L 1 -I ITEM {bsh_script_file} "ITEM" {ssh_host_cmd_end}'
    )

    # cmdStr = (
    #    f"source {condash_location} && conda activate {args.custom_conda_env}/ && "
    #    f"{qsubarray} xargs -I FILEPATH sh -c '{absolute_path_wrapper} {plot_script} {p_id} $0 "
    #    f"../{p_id} "
    #    f"{p_id} "
    #    f"-i $0 "
    #    f"-o $(dirname $0)/../{p_id}' FILEPATH"
    # )
    logger.info("Starting %s plugin execution..." % args.plugin)
    p = Popen(cmdStr, stdin=PIPE, shell=True, executable="/bin/bash")
    p.communicate(input=files_infos_str.encode("utf-8"))
    logger.info("Finished %s execution." % args.plugin)
