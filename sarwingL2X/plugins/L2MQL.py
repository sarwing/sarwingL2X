import logging

logger = logging.getLogger(__name__)

from . import PluginArgs
from ..constants import *
from sarwingL2X import checkStatus, checkVersion
import os
import owiQL
import csv
from subprocess import Popen, PIPE


def L2MQL(args: PluginArgs):
    logger.info("Starting L2MQL plugin")
    post_proc_dir = args.config["post_processing_dir"]

    use_qsubarray = args.plugin_config.get("use_qsubarray", args.use_qsubarray_default)
    ssh_host = args.plugin_config.get("ssh_host", args.ssh_host_default)

    if use_qsubarray:
        qsubarray = args.plugin_config.get("qsubarray", os.getenv("QSUBARRAY", ""))
    else:
        qsubarray = ""

    if ssh_host:
        condash_location = args.config["condash_source_location_remote"]
        ssh_host_cmd = f"rsh {ssh_host} 'bash -c \""
        ssh_host_cmd_end = "\"'"
    else:
        condash_location = args.config["condash_source_location"]
        ssh_host_cmd = ""
        ssh_host_cmd_end = ""

    QLforce = args.force
    dirsToProcess = []
    reportDirs = []
    reportRes = []
    reportDirsDebug = []
    p_name = "L2MQL"
    with open(os.path.join(args.report_path, MULTI_RES_CSV), "r") as csvReport:
        reportDict = csv.DictReader(csvReport)
        for row in reportDict:
            dirPath = os.path.dirname(row["L2M file"])
            res = row["Resolution"]
            comments = row["Comments"]
            statusFile = "%s/L2M%s.status" % (dirPath, res)
            if os.path.isfile(statusFile):
                with open(statusFile, "r") as status:
                    code = status.readline().strip()
                    if code == "0":
                        QLstatusPath = os.path.join(
                            dirPath, post_proc_dir, f"{p_name}{res}", f"{p_name}.status"
                        )
                        QLversionPath = os.path.join(
                            dirPath,
                            post_proc_dir,
                            f"{p_name}{res}",
                            f"{p_name}.version",
                        )
                        multiResReportPath = (
                            dirPath + f"/{post_proc_dir}/multiResSummary.html"
                        )
                        if (
                            QLforce
                            or checkStatus(QLstatusPath)
                            or checkVersion(QLversionPath, owiQL.version)
                        ):
                            dirsToProcess.append("%s %s" % (dirPath, res))
                            pathCommentDict = {"path": dirPath, "comments": comments}
                            if pathCommentDict not in reportDirs:
                                reportDirs.append(pathCommentDict)
                        elif not os.path.isfile(multiResReportPath):
                            pathCommentDict = {"path": dirPath, "comments": comments}
                            if pathCommentDict not in reportDirs:
                                reportDirs.append(pathCommentDict)
                        # reportDirsDebug.append({"path" : dirPath, "comments" : comments})
                        if res not in reportRes:
                            reportRes.append(res)
    dirsStr = "\n".join(dirsToProcess)
    if dirsStr != "":
        dirsStr += "\n"
        cmdStr = (
            f"{ssh_host_cmd} {args.post_ssh_cmd} source {condash_location} && conda activate "
            f"{args.custom_conda_env}/ && {qsubarray} xargs -L 1 -r swMultiResQLWrapper.sh {ssh_host_cmd_end}"
        )
        logger.info("Executing swMultiResQLWrapper.sh...")
        p = Popen(cmdStr, stdin=PIPE, shell=True, executable="/bin/bash")
        p.communicate(input=dirsStr.encode("utf-8"))
        logger.info("Finished executing swMultiResQLWrapper.sh")

    logger.info("Building multiRes summary reports...")
    multiResReportInput = []

    # for d in reportDirsDebug:
    for d in reportDirs:
        multiResReportInput.append(
            "-m %s -i %s -c %s" % (" ".join(reportRes), d["path"], d["comments"])
        )
    strInput = "\n".join(multiResReportInput)
    if strInput != "":
        strInput += "\n"
        cmdStr = (
            f"{ssh_host_cmd} {args.post_ssh_cmd} source {condash_location} && conda activate {args.custom_conda_env}/ && "
            f"{qsubarray} xargs -r swMultiResReport.py {ssh_host_cmd_end}"
        )
        p = Popen(cmdStr, stdin=PIPE, shell=True, executable="/bin/bash")
        p.communicate(input=strInput.encode("utf-8"))
    # DONT FORGET : if theres no logs it's because multiResReport includes only newly generated QLs
    logger.info("Finished building multiRes summary reports.")
